### Access Levels

##### There are several access levels for each role, not necessarily inherited by a higher role

1. Admin
2. Teachers / Faculty
3. Students

##### Activities are divided amongst sections of processes done by OSA

1. General School Processes that consist of:

   1. User Registration
   2. Profiles, split between:
      1. Viewing
      2. Editing
   3. Retrieval of previous Grades

2. Pre - Enrollment Processes that consist of:

   1. Populating Subjects, split between:
      1. Adding Subjects
      2. Checking Available Subjects
   2. Student Assessment
   3. Reserving slots for Subjects

3. Enrollment Processes that consist of:

   1. School Schedule Building
   2. Class Inclusion (Inserting Students into classes)
   3. Viewing Enrollment Details / Class Schedule Details for Teachers

4. Subject Information

   1. Inputting Grades
   2. Retrieving Grades

5. Viewing News

   1. Retrieving / Viewing News List
   2. Adding News

6. Conversations

   1. Joining Group Chat
   2. Private Chat
   3. Viewing Chat logs

7. Files Exchange
   1. Uploading Files
   2. Downloading Files

Activities allowed to Admins:

Virtually everything (GET, POST, PATCH, DELETE) except activities meant to be done by the students themselves (Sections 1.2.2, 2.3, 6.1, 6.2, 7.1) and by the Faculty (Sections 1.2, 4.1, 6.1, 6.2, 7.1) (ONLY GET AND DELETE ACCESS)

Activities allowed to students:

- View Profiles of Everyone (Section 1.2.1) (GET)
- Edit Own Profiles (Section 1.2.2) (PATCH)
- Get Assessment (Section 2.2) (GET)
- Reserve Slots for a Class (Section 2.3) (GET / POST / PATCH)
- View Enrollment Details (Section 3.3) (GET)
- View Current Grades on a Subject (Section 4.2) (GET)
- View news (both general news and ones specific to their class) (Section 5.1) (GET)
- Participate in private chat(? try to implement using chat kit) and specific rooms intended for use in enrolled classes (Section 6.1, 6.2) (GET - WEBSOCKETS)
- Upload files in a space open only to classes enrolled (Section 7.1, Asymmetric, Only Teacher in that class can view) (POST File - Form)
- Download files uploaded by their teacher in a specific class (Section 7.2) (GET)

Activities allowed to Faculty / Teachers:

- View profiles of everyone (Section 1.2.1) (GET)
- Edit own profiles (Section 1.2.2) (PATCH)
- Retrieval of previous grades on previously taught subjects (Section 1.3) (GET)
- Submitting a schedule for availability (?) (Section 3.1) (POST)
- Checking Class Schedule (Section 3.3) (GET)
- Inputting Grades (Section 4.1) (POST / PATCH)
- Retrieving Grades (Section 4.2) (GET)
- Viewing News (Section 5.1) (GET)
- Adding Class Specific News (Section 5.2) (POST / PATCH)
- Participate in private chat(? try to implement using chat kit) and specific rooms intended for use in enrolled classes (Section 6.1, 6.2) (GET - WEBSOCKETS)
- Uploading Files to Specific Classes (Section 7.1) (POST Form - File)
- Downloading Files from Specific Classes (Section 7.2) (GET)
