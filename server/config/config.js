const Student = require(`../models/Student`);
const Teacher = require(`../models/Teacher`);
const Staff = require(`../models/Staff`);
const passport = require(`passport`);

passport.serializeUser((user, done) => {
  done(null, user.id);
});

passport.deserializeUser((id, done) => {
  Student.findById(id)
    .then(studentUser => {
      if (!studentUser) {
        return Teacher.findById(id).then(teacherUser => {
          if (!teacherUser) {
            return Staff.findById(id).then(staffUser => {
              done(null, staffUser);
            });
          }
          return done(null, teacherUser);
        });
      }
      return done(null, studentUser);
    })
    .catch(err => done(err));
});
