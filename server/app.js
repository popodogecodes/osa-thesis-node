const express = require(`express`);
const home = require(`./routes/client/index`);
const cors = require(`cors`);
const mongoose = require(`mongoose`);

const app = express();

const port = process.env.PORT || 3000;

mongoose.connect(`mongodb://localhost:27017/OSA`, {
  useCreateIndex: true,
  useNewUrlParser: true,
});

require(`./config/config`);
app.use(cors({ origin: [`http://localhost:8080`, `http://0.0.0.0:8080`], credentials: true }));
app.use(`/`, home);

app.listen(port, () => {
  console.log(`express server is running at port ${port}`);
});

module.exports = app;
