const router = require(`express`).Router();
const _ = require(`lodash`);
const Curriculum = require(`../../models/Curriculum`);
const Subject = require(`../../models/Subject`);
const bodyParser = require(`body-parser`);

const programHeadOnly = (req, res, next) => {
  if (req.user.role === `Program Head`) {
    next();
  } else {
    res.status(404).send();
  }
};

router.use(bodyParser.json());
router.use(bodyParser.urlencoded({ extended: false }));

router.get(`/`, async (req, res) => {
  const curriculums = await Curriculum.find({}, `-subjects`);
  res.json(curriculums.map(value => value.toObject()));
});

router.post(`/`, programHeadOnly, async (req, res) => {
  try {
    delete req.body.id;
    const curriculum = await Curriculum.create(req.body);
    res.json(curriculum);
  } catch (err) {
    res.status(400).send();
  }
});

router.post(`/search`, async (req, res) => {
  const queryObj = _.pick(req.body, [`year`, `course`, `id`]);
  let curriculums;
  if (queryObj.id) {
    curriculums = [await Curriculum.findById(queryObj.id, `-subjects`)];
  } else {
    curriculums = await Curriculum.find(queryObj, `-subjects`);
  }
  res.json(curriculums.map(value => value.toObject()));
});

router.get(`/:id`, async (req, res) => {
  const curriculum = await Curriculum.findById(req.params.id);

  const payload = await Curriculum.populate(curriculum, {
    path: `subjects`,
    model: Subject,
  });

  res.json(payload);
});

router.patch(`/:id`, programHeadOnly, async (req, res) => {
  try {
    const curriculum = await Curriculum.findById(req.params.id);
    const options = _.pick(req.body, [`year`, `course`, `subjects`]);
    Object.keys(options).forEach(key => {
      curriculum[key] = options[key];
    });
    res.json(await curriculum.save());
  } catch (err) {
    res.status(400).send();
  }
});

router.delete(`/:id`, programHeadOnly, async (req, res) => {
  await Curriculum.findByIdAndDelete(req.params.id);
  res.status(200).send();
});

module.exports = router;
