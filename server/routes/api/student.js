const router = require(`express`).Router();
const _ = require(`lodash`);
const Student = require(`../../models/Student`);
const Classes = require(`../../models/Class`);
const Professor = require(`../../models/Teacher`);
const Subject = require(`../../models/Subject`);
const Grade = require(`../../models/Grade`);
const Semester = require(`../../models/Semester`);
const Curriculum = require(`../../models/Curriculum`);
const bodyParser = require(`body-parser`);
const B2 = require(`backblaze-b2`);
const multer = require(`multer`);

const storage = multer.memoryStorage();
const upload = multer({ storage });

const b2 = new B2({
  accountId: `4b7a2d824f3d`,
  applicationKey: `002a96e2471d3f91231cc2e55efea065feefb55934`,
});

async function uploadFileToB2(file) {
  try {
    await b2.authorize();
    const response = await b2.getUploadUrl({
      bucketId: `a4ebc7ea227d3812649f031d`,
    });

    return b2
      .uploadFile({
        uploadUrl: response.data.uploadUrl,
        uploadAuthToken: response.data.authorizationToken,
        fileName: file.originalname,
        data: file.buffer,
      })
      .then(bucketFile => bucketFile.data);
  } catch (err) {
    console.log(`Something happened!:`, err);
    throw new Error();
  }
}

const queryOptions = require(`../../controllers/middleware/setAuthorizationQueryOptions`);

const authorizedPeopleOnly = (req, res, next) => {
  if (req.user.role === `Registrar`) {
    next();
  } else if (req.user.id === req.params.id) {
    delete req.body.classes;
    delete req.body.curriculum;
    delete req.body.enrollmentStatus;
    next();
  } else {
    res.status(404).send();
  }
};

const registrarOnly = (req, res, next) => {
  if (req.user.role === `Registrar`) {
    next();
  } else {
    res.status(404).send();
  }
};

const cashierOnly = (req, res, next) => {
  if (req.user.role === `Cashier`) {
    next();
  } else {
    res.status(404).send();
  }
};

router.use(bodyParser.json());
router.use(bodyParser.urlencoded({ extended: false }));

router.get(`/`, queryOptions, async (req, res) => {
  const students = await Student.find({ _id: { $ne: req.user.id } }, res.locals.queryOptions.student);
  res.json(students.map(doc => doc.toObject()));
});

router.get(`/stats`, registrarOnly, async (req, res) => {
  const totalStudentCount = await Student.countDocuments({});
  const enrolledStudents = await Student.find({ 'enrollmentStatus.isCurrentlyEnrolled': true }).populate({
    path: `curriculum`,
    model: Curriculum,
  });

  const uniqueCourses = [...new Set(enrolledStudents.map(student => student.curriculum.course))];

  const studentsByCourseAndYear = [];

  await Promise.all(
    uniqueCourses.map(course =>
      Curriculum.findOne({ course }).then(curriculum => {
        const studentCount = [];
        for (let i = 0; i < curriculum.subjects.length; i += 1) {
          if (i % 2 === 0) {
            const studentByYearCount = enrolledStudents.filter(
              student => student.yearLevel + 1 === i && student.curriculum.course === course,
            ).length;
            studentCount.push(studentByYearCount);
          }
        }
        studentsByCourseAndYear.push({ course, studentCount });
      }),
    ),
  );

  const payload = {
    total: totalStudentCount,
    enrolled: enrolledStudents.length,
    courses: studentsByCourseAndYear,
  };

  res.json(payload);
});

router.post(`/`, registrarOnly, async (req, res) => {
  delete req.body.id;
  const student = await Student.create(req.body);
  res.json(student);
});

router.post(`/search`, queryOptions, async (req, res) => {
  const queryObj = _.pick(req.body, [`id`, `firstName`, `middleName`, `lastName`]);
  let students;
  if (queryObj.id) {
    students = [await Student.findById(queryObj.id, res.locals.queryOptions.student)];
  } else {
    students = await Student.find({ _id: { $ne: req.user.id }, ...queryObj }, res.locals.queryOptions.student);
  }
  res.json(students.map(doc => doc.toObject()));
});

router.get(`/:id`, queryOptions, async (req, res) => {
  const student = await Student.findById(req.params.id, res.locals.queryOptions.student);
  const payload = await Student.populate(student, [
    {
      path: `classes`,
      model: Classes,
      populate: [
        {
          path: `professor`,
          model: Professor,
          select: res.locals.queryOptions.teacher,
        },
        {
          path: `subject`,
          model: Subject,
        },
        {
          path: `attendees.student`,
          model: Student,
          select: `firstName middleName lastName`,
        },
        {
          path: `attendees.grade`,
          model: Grade,
          match: { student: student.id },
        },
        {
          path: `semester`,
          model: Semester,
          select: `-classes`,
        },
      ],
    },
    {
      path: `curriculum`,
      model: Curriculum,
      populate: {
        path: `subjects`,
        model: Subject,
      },
    },
  ]);
  res.json(payload.toObject());
});

router.get(`/:id/assessment`, authorizedPeopleOnly, async (req, res) => {
  const student = await Student.findById(req.params.id);
  const assessment = await student.assessment();
  res.json(assessment);
});

router.patch(`/:id`, authorizedPeopleOnly, upload.single(`attachment`), async (req, res) => {
  const student = await Student.findById(req.params.id);

  if (req.file) {
    if (student.attachment) {
      await File.findByIdAndRemove(student.attachment);
    }
    const attachment = req.file;
    const attachmentData = await uploadFileToB2(attachment);
    const attachmentFile = await File.create({
      fileId: attachmentData.fileId,
      size: attachment.size,
      fileName: attachment.originalname,
    });

    student.attachment = attachmentFile._id;
  }

  const options = _.pick(req.body, [
    `firstName`,
    `middleName`,
    `lastName`,
    `birthday`,
    `gender`,
    `houseNumber`,
    `street`,
    `barangay`,
    `city`,
    `province`,
    `civilStatus`,
    `form137A`,
    `form138`,
    `GMC`,
    `ALS`,
    `NCAEResult`,
    `BC`,
    `TOR`,
    `TCG`,
    `honorableDismissal`,
    `pictures`,
  ]);
  Object.keys(options).forEach(key => {
    student[key] = options[key];
  });
  res.json(await student.save());
});

router.patch(`/:id/payment`, queryOptions, cashierOnly, async (req, res) => {
  try {
    const student = await Student.findById(req.params.id, res.locals.queryOptions.student);
    const options = _.pick(req.body.enrollmentStatus, [
      `isCurrentlyEnrolled`,
      `hasPaidPrelims`,
      `hasPaidMidterms`,
      `hasPaidFinals`,
    ]);
    Object.keys(options).forEach(key => {
      student.enrollmentStatus[key] = options[key];
    });
    await student.save();
    res.json(student.toObject());
  } catch (err) {
    res.status(400).send();
  }
});

router.delete(`/:id`, registrarOnly, async (req, res) => {
  await Student.findByIdAndDelete(req.params.id);
  res.status(200).send();
});

module.exports = router;
