const router = require(`express`).Router();
const _ = require(`lodash`);
const bodyParser = require(`body-parser`);

router.use(bodyParser.json());
router.use(bodyParser.urlencoded({ extended: false }));

const Information = require(`../../models/Information`);

router.get(`/`, async (req, res) => {
  const info = await Information.findOne({});

  res.json(info);
});

router.patch(`/`, async (req, res) => {
  try {
    const info = await Information.findOne({});

    const options = _.pick(req.body, [`phoneNumber`, `website`, `address`]);

    Object.keys(options).forEach(key => {
      info[key] = options[key];
    });

    res.json(await info.save());
  } catch (err) {
    res.status(400).send();
  }
});

module.exports = router;
