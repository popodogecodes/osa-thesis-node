const router = require(`express`).Router();
const _ = require(`lodash`);
const Subject = require(`../../models/Subject`);
const bodyParser = require(`body-parser`);

router.use(bodyParser.json());
router.use(bodyParser.urlencoded({ extended: false }));

const programHeadOnly = (req, res, next) => {
  if (req.user.role === `Program Head`) {
    next();
  } else {
    res.status(404).send();
  }
};

router.get(`/`, async (req, res) => {
  const subjects = await Subject.find({});
  res.json(subjects);
});

router.post(`/`, programHeadOnly, async (req, res) => {
  delete req.body.id;
  const subject = await Subject.create(req.body);
  return res.json(subject);
});

router.post(`/search`, async (req, res) => {
  const queryObj = _.pick(req.body, [`name`, `classCode`]);
  const subjects = await Subject.find(queryObj);
  res.json(subjects);
});

router.get(`/:id`, async (req, res) => {
  const subject = await Subject.findById(req.params.id).populate({
    path: `prerequisites.subjects`,
    select: `name classCode`,
  });
  res.json(subject.toObject());
});

router.patch(`/:id`, programHeadOnly, async (req, res) => {
  const subject = await Subject.findById(req.params.id);
  const options = _.pick(req.body, [`name`, `classCode`, `isCountedGPA`, `units`, `prerequisites`]);
  Object.keys(options).forEach(key => {
    subject[key] = options[key];
  });
  res.json(await subject.save());
});

router.delete(`/:id`, programHeadOnly, async (req, res) => {
  await Subject.findByIdAndDelete(req.params.id);
  res.status(200).send();
});

module.exports = router;
