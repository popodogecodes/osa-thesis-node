const router = require(`express`).Router();
const semester = require(`./semester`);
const subject = require(`./subject`);
const student = require(`./student`);
const curriculum = require(`./curriculum`);
const faculty = require(`./faculty`);
const classes = require(`./class`);
const grade = require(`./grade`);
const staff = require(`./staff`);
const schedule = require(`./schedule`);
const section = require(`./section`);
const files = require(`./files`);
const news = require(`./news`);
const information = require(`./information`);

const loginCheck = require(`../../controllers/middleware/libraries/loginCheck`);

router.use(loginCheck);

router.use(`/semester`, semester);
router.use(`/subject`, subject);
router.use(`/student`, student);
router.use(`/curriculum`, curriculum);
router.use(`/faculty`, faculty);
router.use(`/class`, classes);
router.use(`/grade`, grade);
router.use(`/schedule`, schedule);
router.use(`/staff`, staff);
router.use(`/section`, section);
router.use(`/news`, news);
router.use(`/files`, files);
router.use(`/info`, information);

module.exports = router;
