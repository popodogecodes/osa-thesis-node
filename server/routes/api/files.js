const router = require(`express`).Router();
const bodyParser = require(`body-parser`);
const B2 = require(`backblaze-b2`);
// const multer = require(`multer`);

const File = require(`../../models/Files`);

router.use(bodyParser.json());
router.use(bodyParser.urlencoded({ extended: false }));

// const storage = multer.memoryStorage();
// const upload = multer({ storage });

const b2 = new B2({
  accountId: `4b7a2d824f3d`,
  applicationKey: `002a96e2471d3f91231cc2e55efea065feefb55934`,
});

router.get(`/:id`, async (req, res) => {
  try {
    const file = await File.findById(req.params.id);
    await b2.authorize();
    const response = await b2.downloadFileById({
      fileId: file.fileId,
      responseType: `stream`,
    });

    res.set({
      'Content-Disposition': `attachment; filename=${response.headers[`x-bz-file-name`]}`,
      'Content-Type': response.headers[`content-type`],
    });
    response.data.pipe(res);
  } catch (err) {
    console.log(`Something happened!:`, err);
    res.status(500).send();
  }
});

module.exports = router;
