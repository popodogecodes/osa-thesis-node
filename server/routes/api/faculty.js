const router = require(`express`).Router();
const _ = require(`lodash`);
const Teacher = require(`../../models/Teacher`);
const Classes = require(`../../models/Class`);
const Subject = require(`../../models/Subject`);
const Student = require(`../../models/Student`);
const Grade = require(`../../models/Grade`);
const Semester = require(`../../models/Semester`);
const bodyParser = require(`body-parser`);

const queryOptions = require(`../../controllers/middleware/setAuthorizationQueryOptions`);

const authorizedPeopleOnly = (req, res, next) => {
  if (req.user.role === `Program Head`) {
    next();
  } else if (req.user.id === req.params.id) {
    delete req.body.department;
    next();
  } else {
    res.status(404).send();
  }
};

const programHeadOnly = (req, res, next) => {
  if (req.user.role === `Program Head`) {
    next();
  } else {
    res.status(404).send();
  }
};

router.use(bodyParser.json());
router.use(bodyParser.urlencoded({ extended: false }));

router.get(`/`, queryOptions, async (req, res) => {
  const teacher = await Teacher.find({ _id: { $ne: req.user.id } }, res.locals.queryOptions.teacher);
  res.json(teacher.map(doc => doc.toObject()));
});

router.post(`/`, programHeadOnly, async (req, res) => {
  try {
    delete req.body.id;
    const teacher = await Teacher.create(req.body);
    res.json(teacher);
  } catch (err) {
    res.status(400).send();
  }
});

router.post(`/search`, queryOptions, async (req, res) => {
  const queryObj = _.pick(req.body, [`id`, `firstName`, `middleName`, `lastName`]);
  let teachers;
  if (queryObj.id) {
    teachers = [await Teacher.findById(queryObj.id, res.locals.queryOptions.teacher)];
  } else {
    teachers = await Teacher.find({ _id: { $ne: req.user.id }, ...queryObj }, res.locals.queryOptions.teacher);
  }
  res.json(teachers.map(doc => doc.toObject()));
});

router.get(`/:id`, queryOptions, async (req, res) => {
  const teacher = await Teacher.findById(req.params.id, res.locals.queryOptions.teacher);

  const payload = await Teacher.populate(teacher, {
    path: `classes`,
    model: Classes,
    populate: [
      {
        path: `professor`,
        model: Teacher,
        select: res.locals.queryOptions.teacher,
      },
      {
        path: `subject`,
        model: Subject,
      },
      {
        path: `attendees.student`,
        model: Student,
        select: res.locals.queryOptions.student,
      },
      {
        path: `attendees.grade`,
        model: Grade,
      },
      {
        path: `semester`,
        model: Semester,
      },
    ],
  });

  res.json(payload.toObject());
});

router.patch(`/:id`, authorizedPeopleOnly, async (req, res) => {
  try {
    const teacher = await Teacher.findById(req.params.id);
    const options = _.pick(req.body, [
      `firstName`,
      `middleName`,
      `lastName`,
      `birthday`,
      `gender`,
      `houseNumber`,
      `street`,
      `barangay`,
      `city`,
      `province`,
      `civilStatus`,
    ]);
    if (req.user.role === `Program Head`) {
      options.concat(_.pick(req.body, [`department`, `isCurrentlyEmployed`]));
    }
    Object.keys(options).forEach(key => {
      teacher[key] = options[key];
    });
    res.json(await teacher.save());
  } catch (err) {
    res.status(400).send();
  }
});

router.delete(`/:id`, programHeadOnly, async (req, res) => {
  await Teacher.findByIdAndDelete(req.params.id);
  res.status(200).send();
});

module.exports = router;
