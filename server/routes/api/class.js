const router = require(`express`).Router();
const _ = require(`lodash`);
const Classes = require(`../../models/Class`);
const Student = require(`../../models/Student`);
const Subject = require(`../../models/Subject`);
const Semester = require(`../../models/Semester`);
const Teacher = require(`../../models/Teacher`);
const Grade = require(`../../models/Grade`);
const Section = require(`../../models/Section`);
const bodyParser = require(`body-parser`);
const queryOptions = require(`../../controllers/middleware/setAuthorizationQueryOptions`);

const programHeadOnly = (req, res, next) => {
  if (req.user.role === `Program Head`) {
    next();
  } else {
    res.status(404).send();
  }
};

router.use(bodyParser.json());
router.use(bodyParser.urlencoded({ extended: false }));

router.get(`/`, async (req, res) => {
  const classes = await Classes.find({}, `subject schedule semester`).populate([
    { path: `subject`, model: Subject },
    { path: `semester`, model: Semester, select: `schoolYear instance` },
  ]);
  res.json(classes.map(value => value.toObject()));
});

router.post(`/`, programHeadOnly, async (req, res) => {
  try {
    const { professor, subject, schedule, semester } = req.body;
    const classes = await Classes.create({ professor, subject, schedule, semester });
    res.json(classes);
  } catch (err) {
    res.status(400).send();
  }
});

router.post(`/search`, async (req, res) => {
  const queryObj = _.pick(req.body, [`subject`, `semester`, `id`]);
  let classes;
  if (queryObj.id) {
    classes = [
      await Classes.findById(queryObj.id, `subject schedule semester`).populate([
        { path: `subject`, model: Subject },
        { path: `semester`, model: `Semester`, select: `schoolYear instance` },
      ]),
    ];
  } else {
    classes = await Classes.find(queryObj, `subject schedule semester`).populate([
      { path: `subject`, model: Subject },
      { path: `semester`, model: `Semester`, select: `schoolYear instance` },
    ]);
  }

  res.json(classes.map(value => value.toObject()));
});

router.get(`/:id`, queryOptions, async (req, res) => {
  const classes = await Classes.findById(req.params.id);

  let isStudentInClass = false;
  classes.attendees.forEach(attendee => {
    attendee.student.toString() === req.user.id ? (isStudentInClass = true) : isStudentInClass;
  });

  if (classes.professor.toString() === req.user.id) {
    // if the professor is accessing his class..
    const payload = await Classes.populate(classes, [
      {
        path: `subject`,
        model: Subject,
      },
      {
        path: `semester`,
        model: Semester,
      },
      { path: `professor`, model: Teacher, select: res.locals.queryOptions.teacher },
      {
        path: `attendees.student`,
        model: Student,
        select: res.locals.queryOptions.student,
      },
      {
        path: `attendees.grade`,
        model: Grade,
      },
    ]);
    res.json(payload.toObject());
  } else if (req.user.role === `Program Head` || req.user.role === `Registrar` || isStudentInClass) {
    // if the user is a Program Head, a Registrar, or a student in this class..
    const payload = await Classes.populate(classes, [
      { path: `professor`, model: Teacher, select: res.locals.queryOptions.teacher },
      { path: `subject`, model: Subject },
      { path: `semester`, model: Semester },
      { path: `attendees.student`, model: Student, select: res.locals.queryOptions.student },
      { path: `attendees.grade`, model: Grade },
    ]);
    res.json(payload.toObject());
  } else {
    const payload = await Classes.findById(req.params.id, `-attendees`).populate([
      { path: `professor`, model: Teacher, select: res.locals.queryOptions.teacher },
      { path: `subject`, model: Subject },
      { path: `semester`, model: Semester },
    ]);
    res.json(payload.toObject());
  }
});

router.patch(`/:id/professor`, programHeadOnly, async (req, res) => {
  try {
    if (req.body.id) {
      const newProf = req.body.id;

      let classes = await Classes.findById(req.params.id);
      classes = await classes.replaceProfessor(newProf.id);

      res.json(classes);
    } else {
      res.status(400).send();
    }
  } catch (err) {
    res.status(400).send();
  }
});

router.delete(`/:id`, programHeadOnly, async (req, res) => {
  await Classes.findByIdAndRemove(req.params.id);
  res.json();
});

router.patch(`/:id/student`, programHeadOnly, async (req, res) => {
  try {
    if (req.body.id) {
      let classes = await Classes.findById(req.params.id);
      classes = await classes.addStudent(req.body.id);

      res.json(classes);
    } else {
      res.status(400).send();
    }
  } catch (err) {
    res.status(400).send();
  }
});

router.patch(`/:id/section`, programHeadOnly, async (req, res) => {
  try {
    if (req.body.id) {
      const section = await Section.findById(req.body.id);
      let classes = await Classes.findById(req.params.id);
      await Promise.all(
        section.students
          .map(student => student.toString())
          .filter(student => !classes.attendees.map(attendee => attendee.student.toString()).includes(student))
          .map(student => classes.addStudent(student)),
      );
      classes = await Classes.findById(req.params.id);

      res.json(classes);
    } else {
      res.status(400).send();
    }
  } catch (err) {
    res.status(400).send();
  }
});

router.delete(`/:id/section`, programHeadOnly, async (req, res) => {
  try {
    if (req.body.id) {
      const section = await Section.findById(req.body.id);
      let classes = await Classes.findById(req.params.id);
      await Promise.all(
        section.students
          .map(student => student.toString())
          .filter(student => classes.attendees.map(attendee => attendee.student.toString()).includes(student))
          .map(student => classes.removeStudent(student)),
      );
      classes = await Classes.findById(req.params.id);

      res.json(classes);
    } else {
      res.status(400).send();
    }
  } catch (err) {
    res.status(400).send();
  }
});

router.delete(`/:id/student`, programHeadOnly, async (req, res) => {
  if (req.body.id) {
    let classes = await Classes.findById(req.params.id);
    classes = await classes.removeStudent(req.body.id);

    res.json(classes);
  } else {
    res.status(400).send();
  }
});

router.patch(`/:id/schedule`, programHeadOnly, async (req, res) => {
  try {
    if (req.body.schedule) {
      const classes = await Classes.findById(req.params.id);
      classes.schedule = req.body.schedule;
      res.json(await classes.save());
    } else {
      res.status(400).send();
    }
  } catch (err) {
    res.status(400).send();
  }
});

module.exports = router;
