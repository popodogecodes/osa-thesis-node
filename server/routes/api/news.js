const router = require(`express`).Router();
const bodyParser = require(`body-parser`);
const _ = require(`lodash`);
const B2 = require(`backblaze-b2`);
const multer = require(`multer`);

const News = require(`../../models/News`);
const Student = require(`../../models/Student`);
const Teacher = require(`../../models/Teacher`);
const Staff = require(`../../models/Staff`);
const Curriculum = require(`../../models/Curriculum`);
const File = require(`../../models/Files`);

router.use(bodyParser.json());
router.use(bodyParser.urlencoded({ extended: false }));

const storage = multer.memoryStorage();
const upload = multer({ storage });

const b2 = new B2({
  accountId: `4b7a2d824f3d`,
  applicationKey: `002a96e2471d3f91231cc2e55efea065feefb55934`,
});

async function uploadFileToB2(file) {
  try {
    await b2.authorize();
    const response = await b2.getUploadUrl({
      bucketId: `a4ebc7ea227d3812649f031d`,
    });

    return b2
      .uploadFile({
        uploadUrl: response.data.uploadUrl,
        uploadAuthToken: response.data.authorizationToken,
        fileName: file.originalname,
        data: file.buffer,
      })
      .then(bucketFile => bucketFile.data);
  } catch (err) {
    console.log(`Something happened!:`, err);
    throw new Error();
  }
}

router.post(`/personal`, async (req, res) => {
  if (req.body.id) {
    if (req.body.group === `student`) {
      const student = await Student.findById(req.body.id).populate({
        path: `curriculum`,
        model: Curriculum,
      });

      let publicNews = await News.find({ isPrivate: false });
      publicNews = await Promise.all(
        publicNews.map(singleArticle => {
          if (singleArticle.authorType === `faculty`) {
            return News.populate(singleArticle, {
              path: `author`,
              model: Teacher,
              select: `firstName middleName lastName`,
            });
          }
          return News.populate(singleArticle, {
            path: `author`,
            model: Staff,
            select: `firstName middleName lastName`,
          });
        }),
      );
      let privateNews = await News.find({
        isPrivate: true,
        isStaffLevel: false,
        isFacultyLevel: false,
        'forClass.0': { $exists: false },
        'forCourse.0': { $exists: false },
      });
      privateNews = await Promise.all(
        privateNews.map(singleArticle => {
          if (singleArticle.authorType === `faculty`) {
            return News.populate(singleArticle, {
              path: `author`,
              model: Teacher,
              select: `firstName middleName lastName`,
            });
          }
          return News.populate(singleArticle, {
            path: `author`,
            model: Staff,
            select: `firstName middleName lastName`,
          });
        }),
      );
      let courseNews = await News.find({
        isPrivate: true,
        isStaffLevel: false,
        isFacultyLevel: false,
        'forClass.0': { $exists: false },
      })
        .where(`forCourse`)
        .in([student.curriculum.course]);
      courseNews = await Promise.all(
        courseNews.map(singleArticle => {
          if (singleArticle.authorType === `faculty`) {
            return News.populate(singleArticle, {
              path: `author`,
              model: Teacher,
              select: `firstName middleName lastName`,
            });
          }
          return News.populate(singleArticle, {
            path: `author`,
            model: Staff,
            select: `firstName middleName lastName`,
          });
        }),
      );
      let classNews = await News.find({
        isPrivate: true,
        isStaffLevel: false,
        isFacultyLevel: false,
        'forCourse.0': { $exists: false },
      })
        .where(`forClass`)
        .in(student.classes);
      classNews = await Promise.all(
        classNews.map(singleArticle => {
          if (singleArticle.authorType === `faculty`) {
            return News.populate(singleArticle, {
              path: `author`,
              model: Teacher,
              select: `firstName middleName lastName`,
            });
          }
          return News.populate(singleArticle, {
            path: `author`,
            model: Staff,
            select: `firstName middleName lastName`,
          });
        }),
      );
      const news = [].concat(publicNews, privateNews, courseNews, classNews);

      res.json(news.map(singleArticle => singleArticle.toObject()));
    } else if (req.body.group === `faculty`) {
      let publicNews = await News.find({ isPrivate: false });
      publicNews = await Promise.all(
        publicNews.map(singleArticle => {
          if (singleArticle.authorType === `faculty`) {
            return News.populate(singleArticle, {
              path: `author`,
              model: Teacher,
              select: `firstName middleName lastName`,
            });
          }
          return News.populate(singleArticle, {
            path: `author`,
            model: Staff,
            select: `firstName middleName lastName`,
          });
        }),
      );
      let privateNews = await News.find({ isPrivate: true, isStaffLevel: false });
      privateNews = await Promise.all(
        privateNews.map(singleArticle => {
          if (singleArticle.authorType === `faculty`) {
            return News.populate(singleArticle, {
              path: `author`,
              model: Teacher,
              select: `firstName middleName lastName`,
            });
          }
          return News.populate(singleArticle, {
            path: `author`,
            model: Staff,
            select: `firstName middleName lastName`,
          });
        }),
      );
      const news = [].concat(privateNews, publicNews);

      res.json(news.map(singleArticle => singleArticle.toObject()));
    } else if (req.body.group === `staff`) {
      let news = await News.find({});
      news = await Promise.all(
        news.map(singleArticle => {
          if (singleArticle.authorType === `faculty`) {
            return News.populate(singleArticle, {
              path: `author`,
              model: Teacher,
              select: `firstName middleName lastName`,
            });
          }
          return News.populate(singleArticle, {
            path: `author`,
            model: Staff,
            select: `firstName middleName lastName`,
          });
        }),
      );
      res.json(news.map(singleArticle => singleArticle.toObject()));
    }
  } else {
    res.status(400).send();
  }
});

router.get(`/:id`, async (req, res) => {
  let news = await News.findById(req.params.id);

  if (news.authorType === `faculty`) {
    news = await News.populate(news, {
      path: `author`,
      model: Teacher,
    });
  } else if (news.authorType === `staff`) {
    news = await News.populate(news, {
      path: `author`,
      model: Staff,
    });
  }

  res.json(news);
});

router.use(async (req, res, next) => {
  if (req.session.group === `faculty` || req.session.group === `staff`) {
    next();
  } else {
    res.status(400).send();
  }
});

router.post(`/`, upload.single(`attachment`), async (req, res) => {
  try {
    let news = new News();

    if (req.file) {
      const attachment = req.file;
      const attachmentData = await uploadFileToB2(attachment);
      const attachmentFile = await File.create({
        fileId: attachmentData.fileId,
        size: attachment.size,
        fileName: attachment.originalname,
      });

      news.attachment = attachmentFile._id;
    }

    const {
      title,
      author,
      authorType,
      content,
      isPrivate,
      isStaffLevel,
      isFacultyLevel,
      forClass,
      forCourse,
    } = req.body;

    news.title = title;
    news.author = author;
    news.authorType = authorType;
    news.content = content;
    news.isPrivate = isPrivate;
    news.isStaffLevel = isStaffLevel;
    news.isFacultyLevel = isFacultyLevel;
    news.forClass = forClass;
    news.forCourse = forCourse;

    news = await news.save();
    res.json(news);
  } catch (err) {
    res.status(400).send();
  }
});

router.patch(`/:id`, upload.single(`attachment`), async (req, res) => {
  try {
    const news = await News.findById(req.params.id);

    if (news.authorType === `staff` && req.userGroup === news.authorType) {
      if (news.attachment) {
        await File.findByIdAndRemove(news.attachment);
      }
      if (req.file) {
        const attachment = req.file;
        const attachmentData = await uploadFileToB2(attachment);
        const attachmentFile = await File.create({
          fileId: attachmentData.fileId,
          size: attachment.size,
          fileName: attachment.originalname,
        });

        news.attachment = attachmentFile._id;
      }

      const options = _.pick(req.body, [
        `title`,
        `content`,
        `isPrivate`,
        `isStaffLevel`,
        `isFacultyLevel`,
        `forClass`,
        `forCourse`,
      ]);
      Object.keys(options).forEach(key => {
        news[key] = options[key];
      });
      res.json(await news.save());
    } else if (news.authorType === `faculty`) {
      if (req.userGroup === `staff`) {
        if (req.file) {
          await File.findByIdAndRemove(news.attachment);
          const attachment = req.file;
          const attachmentData = await uploadFileToB2(attachment);
          const attachmentFile = await File.create({
            fileId: attachmentData.fileId,
            size: attachment.size,
            fileName: attachment.originalname,
          });

          news.attachment = attachmentFile._id;
        }

        const options = _.pick(req.body, [
          `title`,
          `content`,
          `isPrivate`,
          `isStaffLevel`,
          `isFacultyLevel`,
          `forClass`,
          `forCourse`,
        ]);
        Object.keys(options).forEach(key => {
          news[key] = options[key];
        });
        res.json(await news.save());
      } else if (req.user._id.toString() === news.author.toString()) {
        if (req.file) {
          await File.findByIdAndRemove(news.attachment);
          const attachment = req.file;
          const attachmentData = await uploadFileToB2(attachment);
          const attachmentFile = await File.create({
            fileId: attachmentData.fileId,
            size: attachment.size,
            fileName: attachment.originalname,
          });

          news.attachment = attachmentFile._id;
        }

        const options = _.pick(req.body, [
          `title`,
          `content`,
          `isPrivate`,
          `isStaffLevel`,
          `isFacultyLevel`,
          `forClass`,
          `forCourse`,
        ]);
        Object.keys(options).forEach(key => {
          news[key] = options[key];
        });
        res.json(await news.save());
      } else {
        res.status(400).send();
      }
    } else {
      res.status(400).send();
    }
  } catch (err) {
    res.status(400).send();
  }
});

router.delete(`/:id`, async (req, res) => {
  const news = News.findById(req.params.id);
  if (news.authorType === `staff` && req.userGroup === news.authorType) {
    await News.findByIdAndRemove(req.params.id);
  } else if (news.authorType === `faculty`) {
    if (req.userGroup === `staff`) {
      await News.findByIdAndRemove(req.params.id);
    } else if (req.user._id.toString() === news.author.toString()) {
      await News.findByIdAndRemove(req.params.id);
    } else {
      res.status(400).send();
    }
  } else {
    res.status(400).send();
  }
  res.json();
});

module.exports = router;
