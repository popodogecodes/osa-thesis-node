const router = require(`express`).Router();
const bodyParser = require(`body-parser`);
const _ = require(`lodash`);

const Section = require(`../../models/Section`);
const Semester = require(`../../models/Semester`);
const Student = require(`../../models/Student`);

const queryOptions = require(`../../controllers/middleware/setAuthorizationQueryOptions`);

const programHeadOnly = (req, res, next) => {
  if (req.user.role === `Program Head`) {
    next();
  } else {
    res.status(404).send();
  }
};

router.use(bodyParser.json());
router.use(bodyParser.urlencoded({ extended: false }));

router.get(`/`, async (req, res) => {
  const sections = await Section.find({}, `-students`).populate({
    path: `semester`,
    model: Semester,
    select: `-classes`,
  });

  res.json(sections.map(section => section.toObject()));
});

router.post(`/`, programHeadOnly, async (req, res) => {
  try {
    const { number, semester, course, yearLevel } = req.body;
    const section = await Section.create({ number, semester, course, yearLevel });
    res.json(section);
  } catch (err) {
    res.status(400).send();
  }
});

router.post(`/search`, async (req, res) => {
  const queryObj = _.pick(req.body, [`number`, `semester`, `course`, `yearLevel`, `id`]);
  let sections;
  if (queryObj.id) {
    sections = [
      await Section.findById(queryObj.id, `-students`).populate({
        path: `semester`,
        model: Semester,
        select: `-classes`,
      }),
    ];
  } else {
    sections = await Section.find({ queryObj }, `-students`).populate({
      path: `semester`,
      model: Semester,
      select: `-classes`,
    });
  }

  res.json(sections.map(section => section.toObject()));
});

router.get(`/:id`, queryOptions, async (req, res) => {
  const section = await Section.findById(req.params.id);

  const payload = await Section.populate(section, [
    {
      path: `semester`,
      model: Semester,
      select: `-classes`,
    },
    {
      path: `students`,
      model: Student,
      select: res.locals.queryOptions.student,
    },
  ]);
  res.json(payload.toObject());
});

router.get(`/:id/eligibleStudents`, programHeadOnly, async (req, res) => {
  const section = await Section.findById(req.params.id);
  await section.assessEligibleStudents();
  const students = await section.findEligibleStudents();
  res.json(students.map(student => student.toObject()));
});

router.patch(`/:id/student`, programHeadOnly, async (req, res) => {
  try {
    if (req.body.id) {
      let section = await Section.findById(req.params.id);
      section = await section.addStudents(...req.body.id);

      res.json(section);
    } else {
      res.status(400).send();
    }
  } catch (err) {
    res.status(400).send();
  }
});

router.patch(`/:id/student/transfer`, programHeadOnly, async (req, res) => {
  try {
    if (req.body.id && req.body.section) {
      const oldSection = await Section.findById(req.params.id);
      const newSection = await Section.findById(req.body.section);

      await Promise.all([oldSection.removeStudents(...req.body.id), newSection.addStudents(...req.body.id)]);

      res.json();
    } else {
      res.status(400).send();
    }
  } catch (err) {
    res.status(400).send();
  }
});

router.delete(`/:id`, programHeadOnly, async (req, res) => {
  await Section.findByIdAndRemove(req.params.id);
  res.json();
});

router.delete(`/:id/student`, programHeadOnly, async (req, res) => {
  if (req.body.id) {
    let section = await Section.findById(req.params.id);
    section = await section.removeStudents(...req.body.id);

    res.json(section);
  } else {
    res.status(400).send();
  }
});

module.exports = router;
