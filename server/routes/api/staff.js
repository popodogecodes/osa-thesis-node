const router = require(`express`).Router();
const _ = require(`lodash`);
const Staff = require(`../../models/Staff`);
const bodyParser = require(`body-parser`);

const queryOptions = require(`../../controllers/middleware/setAuthorizationQueryOptions`);

const authorizedPeopleOnly = (req, res, next) => {
  if (req.user.role === `School Director`) {
    next();
  } else if (req.user.id === req.params.id) {
    delete req.body.role;
    next();
  } else {
    res.status(404).send();
  }
};

const schoolDirectorOnly = (req, res, next) => {
  if (req.user.role === `School Director`) {
    next();
  } else {
    res.status(404).send();
  }
};

router.use(bodyParser.json());
router.use(bodyParser.urlencoded({ extended: false }));

router.get(`/`, queryOptions, async (req, res) => {
  const staffs = await Staff.find({ _id: { $ne: req.user.id } }, res.locals.queryOptions.staff);
  res.json(staffs.map(doc => doc.toObject()));
});

router.post(`/`, schoolDirectorOnly, async (req, res) => {
  try {
    delete req.body.id;
    const staff = await Staff.create(req.body);
    res.json(staff);
  } catch (err) {
    res.status(400).send();
  }
});

router.post(`/search`, async (req, res) => {
  const queryObj = _.pick(req.body, [`id`, `firstName`, `middleName`, `lastName`]);
  let staffs;
  if (queryObj.id) {
    staffs = [await Staff.findById(queryObj.id, res.locals.queryOptions.staff)];
  } else {
    staffs = await Staff.find({ _id: { $ne: req.user.id }, ...queryObj }, res.locals.queryOptions.staff);
  }
  res.json(staffs.map(doc => doc.toObject()));
});

router.get(`/:id`, queryOptions, async (req, res) => {
  const staff = await Staff.findById(req.params.id, res.locals.queryOptions.staff);
  res.json(staff.toObject());
});

router.patch(`/:id`, authorizedPeopleOnly, async (req, res) => {
  try {
    const staff = await Staff.findById(req.params.id);
    const options = _.pick(req.body, [
      `firstName`,
      `middleName`,
      `lastName`,
      `birthday`,
      `gender`,
      `houseNumber`,
      `street`,
      `barangay`,
      `city`,
      `province`,
      `civilStatus`,
    ]);
    if (req.user.role === `School Director`) {
      options.concat(_.pick(req.body, [`role`, `isCurrentlyEmployed`]));
    }
    Object.keys(options).forEach(key => {
      staff[key] = options[key];
    });
    res.json(await staff.save());
  } catch (err) {
    res.status(400).send();
  }
});

router.delete(`/:id`, schoolDirectorOnly, async (req, res) => {
  await Staff.findByIdAndDelete(req.params.id);
  res.status(200).send();
});

module.exports = router;
