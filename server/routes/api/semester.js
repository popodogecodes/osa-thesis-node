const router = require(`express`).Router();
const _ = require(`lodash`);
const Semester = require(`../../models/Semester`);
const Classes = require(`../../models/Class`);
const Teacher = require(`../../models/Teacher`);
const Subject = require(`../../models/Subject`);
const bodyParser = require(`body-parser`);
const queryOptions = require(`../../controllers/middleware/setAuthorizationQueryOptions`);

const schoolDirectorOnly = (req, res, next) => {
  if (req.user.role === `School Director`) {
    next();
  } else {
    res.status(404).send();
  }
};

router.use(bodyParser.json());
router.use(bodyParser.urlencoded({ extended: false }));

router.get(`/`, async (req, res) => {
  const semesters = await Semester.find({}, `schoolYear instance`);
  res.json(semesters.map(value => value.toObject()));
});

router.post(`/`, schoolDirectorOnly, async (req, res) => {
  try {
    delete req.body.id;
    const semester = await Semester.create(req.body);
    res.json(semester);
  } catch (err) {
    res.status(400).send();
  }
});

router.post(`/search`, async (req, res) => {
  const semesters = await Semester.find(req.body, `schoolYear instance`);
  res.json(semesters.map(value => value.toObject()));
});

router.get(`/:id`, queryOptions, async (req, res) => {
  const semester = await Semester.findById(req.params.id);
  const payload = await Semester.populate(semester, {
    path: `classes`,
    model: Classes,
    populate: [
      {
        path: `professor`,
        model: Teacher,
        select: res.locals.queryOptions.teacher,
      },
      {
        path: `subject`,
        model: Subject,
      },
    ],
  });

  res.json(payload.toObject());
});

router.patch(`/:id`, schoolDirectorOnly, async (req, res) => {
  try {
    const semester = await Semester.findById(req.params.id);
    const options = _.pick(req.body, [`startDate`, `endDate`, `schoolYear`, `instance`]);
    Object.keys(options).forEach(key => {
      semester[key] = options[key];
    });
    res.json(await semester.save());
  } catch (err) {
    res.status(400).send();
  }
});

router.delete(`/:id`, schoolDirectorOnly, async (req, res) => {
  await Semester.findByIdAndDelete(req.params.id);
  res.status(200).send();
});

module.exports = router;
