const router = require(`express`).Router();
const _ = require(`lodash`);
const Grade = require(`../../models/Grade`);
const Classes = require(`../../models/Class`);
const bodyParser = require(`body-parser`);

router.use(bodyParser.json());
router.use(bodyParser.urlencoded({ extended: false }));

router.patch(`/:id`, async (req, res) => {
  try {
    const grade = await Grade.findById(req.params.id);
    const payload = await Grade.populate(grade, {
      path: `class`,
      model: Classes,
    });
    if (payload.class.professor.equals(req.user.id) || req.user.role === `Registrar`) {
      const options = _.pick(req.body, [`prelim`, `midterm`, `preFinal`, `final`, `status`]);
      Object.keys(options).forEach(key => {
        grade[key] = options[key];
      });
      res.json(await grade.save());
    } else {
      res.status(404).send();
    }
  } catch (err) {
    res.status(400).send();
  }
});

module.exports = router;
