const router = require(`express`).Router();
const bodyParser = require(`body-parser`);

const scheduleController = require(`../../controllers/schedule`);

router.use(bodyParser.json());
router.use(bodyParser.urlencoded({ extended: false }));

router.post(`/`, scheduleController);

module.exports = router;
