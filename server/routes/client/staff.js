const router = require(`express`).Router();

const uuid = require(`uuid/v4`);
const session = require(`express-session`);
const MongoDBStore = require(`connect-mongodb-session`)(session);
const bodyParser = require(`body-parser`);
const passport = require(`passport`);
const LocalStrategy = require(`passport-local`).Strategy;
const Staff = require(`../../models/Staff`);

const api = require(`../api/api`);

const logout = require(`./logout`);

const loginGetController = require(`../../controllers/login`);
const flashController = require(`../../controllers/middleware/libraries/flash`);
const authenticateController = require(`../../controllers/middleware/libraries/authenticate`);
const landingController = require(`../../controllers/landing`);
const setUserGroupController = require(`../../controllers/middleware/setUserGroup`);
const keepAliveController = require(`../../controllers/middleware/keepAlive`);

const storeGroupToSession = require(`../../controllers/middleware/storeGroupToSession`);
const checkLogin = require(`../../controllers/middleware/libraries/loginCheck`);
const authorizationCheck = require(`../../controllers/middleware/authorizationCheck`);

passport.use(
  `staff`,
  new LocalStrategy({ usernameField: `email`, passReqToCallback: true }, (req, email, password, done) => {
    Staff.findOne({ email })
      .then(user => {
        if (!user || !user.validPassword(password)) {
          return done(null, false, req.flash(`error`, `Incorrect email or password.`));
        }
        return done(null, user);
      })
      .catch(err => done(err));
  }),
);

const store = new MongoDBStore({
  uri: `mongodb://localhost:27017/Sessions`,
  collection: `sessions`,
});

store.on(`error`, err => {
  console.log(err);
});

router.use(bodyParser.json());
router.use(bodyParser.urlencoded({ extended: false }));
router.use(
  session({
    name: `sessionID`,
    store,
    cookie: { maxAge: 1000 * 60 * 60 * 2, httpOnly: true },
    genid: () => uuid(), // use UUIDs for session IDs
    secret: `keyboardio popo`,
    resave: true,
    saveUninitialized: true,
    rolling: true,
  }),
);

router.use(flashController);
router.use(setUserGroupController(`staff`));

router.use(passport.initialize());
router.use(passport.session());

router.get(`/login`, loginGetController);

router.post(`/login`, storeGroupToSession, authenticateController);

router.get(`/home`, checkLogin, authorizationCheck, landingController);

router.use(`/api`, authorizationCheck, api);

router.get(`/keepalive`, authorizationCheck, keepAliveController);

router.use(`/logout`, logout);

module.exports = router;
