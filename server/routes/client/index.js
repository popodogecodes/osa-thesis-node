const router = require(`express`).Router();
const B2 = require(`backblaze-b2`);

const students = require(`./student`);
const faculty = require(`./faculty`);
const staff = require(`./staff`);
const homeController = require(`../../controllers/home`);
const logout = require(`./logout`);

const News = require(`../../models/News`);
const File = require(`../../models/Files`);
const Information = require(`../../models/Information`);

const b2 = new B2({
  accountId: `4b7a2d824f3d`,
  applicationKey: `002a96e2471d3f91231cc2e55efea065feefb55934`,
});

router.use(`/student`, students);
router.use(`/faculty`, faculty);
router.use(`/staff`, staff);
router.get(`/`, homeController);
router.use(`/logout`, logout);

router.get(`/public`, async (req, res) => {
  const news = await News.find({ isPrivate: false });

  res.json(news);
});

router.get(`/info`, async (req, res) => {
  const info = await Information.findOne({});

  res.json(info);
});

router.get(`/public/:id`, async (req, res) => {
  const news = await News.findOne({ isPrivate: false, _id: req.params.id });

  if (news === null) {
    res.status(404).send();
  } else {
    res.json(news);
  }
});

router.get(`/public/files/:id`, async (req, res) => {
  try {
    const file = await File.findById(req.params.id);
    await b2.authorize();
    const response = await b2.downloadFileById({
      fileId: file.fileId,
      responseType: `stream`,
    });

    res.set({
      'Content-Disposition': `attachment; filename=${response.headers[`x-bz-file-name`]}`,
      'Content-Type': response.headers[`content-type`],
    });
    response.data.pipe(res);
  } catch (err) {
    console.log(`Something happened!:`, err);
    res.status(500).send();
  }
});

module.exports = router;
