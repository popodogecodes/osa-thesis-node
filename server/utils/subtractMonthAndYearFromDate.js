const subtractMonthAndYearFromDate = (date, month = 0, year = 0) => {
  if (!(date instanceof Date)) {
    return undefined;
  }

  if (!(month || year)) {
    return date;
  }

  if (Number.isNaN(Number(month)) || Number.isNaN(Number(year))) {
    return undefined;
  }

  return new Date(date.setMonth(date.getMonth() - (month + 12 * year)));
};

module.exports = subtractMonthAndYearFromDate;
