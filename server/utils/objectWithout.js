function objectWithout(object, ...fields) {
  const modifiedObj = object;

  fields.forEach(field => delete modifiedObj[field]);

  return modifiedObj;
}

module.exports = objectWithout;
