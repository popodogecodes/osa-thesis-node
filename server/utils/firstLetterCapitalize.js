function titleCase(str) {
  let splitStr = str.toLowerCase().split(` `);
  splitStr = splitStr.map(word => {
    const result = word.charAt(0).toUpperCase() + word.substring(1);
    return result;
  });
  return splitStr.join(` `);
}

module.exports = titleCase;
