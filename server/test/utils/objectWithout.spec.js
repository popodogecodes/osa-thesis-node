const objectWithout = require(`../../utils/objectWithout`);

const { expect } = require(`chai`);
const chai = require(`chai`);
const sinon = require(`sinon`);
const sinonChai = require(`sinon-chai`);

chai.use(sinonChai);

describe(`The objectWithout function`, () => {
  afterEach(async () => {
    sinon.restore();
  });

  it(`should return the object if only one argument is provided`, () => {
    const obj = { a: 1, b: 2 };
    const spy = sinon.spy(objectWithout);
    const result = spy(obj);
    expect(spy).to.have.been.calledOnce;
    expect(spy).to.have.been.calledWith(obj);
    expect(result).to.deep.equal(obj);
  });

  it(`should delete the object property specified in the arguments`, () => {
    const obj = { a: 1, b: 2, c: 3, d: 4 };
    const spy = sinon.spy(objectWithout);
    const result = spy(obj, `b`, `d`);
    expect(spy).to.have.been.calledOnce;
    expect(spy).to.have.been.calledWith(obj, `b`, `d`);
    expect(result).to.deep.equal({ a: 1, c: 3 });
  });
});
