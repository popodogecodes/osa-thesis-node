const { expect } = require(`chai`);
const chai = require(`chai`);
const sinonChai = require(`sinon-chai`);
const sinon = require(`sinon`);

chai.use(sinonChai);

const subtractMonthAndYearFromDate = require(`../../utils/subtractMonthAndYearFromDate`);

describe(`The subtractMonthAndYearFromDate function`, () => {
  const testDate = new Date(2021, 0, 1);
  beforeEach(async () => {
    sinon.useFakeTimers(testDate);
  });

  afterEach(async () => {
    sinon.restore();
  });

  it(`should give a date equivalent to desired date`, () => {
    const date = new Date(2001, 0, 1);
    const targetDate = new Date(2000, 0, 1);
    const result = subtractMonthAndYearFromDate(date, 0, 1);
    expect(result.getTime()).to.equal(targetDate.getTime());
  });

  it(`should return undefined on non-date objects`, () => {
    const result = subtractMonthAndYearFromDate(`not a date`, 0, 1);
    expect(result).to.equal(undefined);
  });

  it(`should return the same date when only provided with the date argument`, () => {
    const date = new Date();
    const result = subtractMonthAndYearFromDate(date);
    expect(result).to.equal(date);
  });

  it(`should return undefined when date or month isn't a number`, () => {
    const date = new Date();
    const result = subtractMonthAndYearFromDate(date, `this`, `cato`);
    const anotherResult = subtractMonthAndYearFromDate(date, 1, `this`);
    expect(result).to.equal(undefined);
    expect(anotherResult).to.equal(undefined);
  });
});
