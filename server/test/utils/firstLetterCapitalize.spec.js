const { expect } = require(`chai`);

const titleCase = require(`../../utils/firstLetterCapitalize`);

describe(`firstLetterCapitalize function`, () => {
  it(`should capitalize every first letter of a word in a string`, () => {
    const sourceStr = `this is a string.`;
    const resultStr = titleCase(sourceStr);
    expect(resultStr).to.not.equal(sourceStr);
    expect(resultStr).to.equal(`This Is A String.`);
  });

  it(`should lowercase characters after the first letter of a word in a string`, () => {
    const sourceStr = `THIS IS IN ALL CAPS`;
    const resultStr = titleCase(sourceStr);
    expect(resultStr).to.not.equal(sourceStr);
    expect(resultStr).to.equal(`This Is In All Caps`);
  });

  it(`should not modify non-alphabet characters in a string`, () => {
    const sourceStr = `This Has Lots Of Different Characters 123123@@!! `;
    const resultStr = titleCase(sourceStr);
    expect(resultStr).to.equal(sourceStr);
  });
});
