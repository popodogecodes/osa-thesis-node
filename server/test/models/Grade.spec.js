const chai = require(`chai`);
const sinonChai = require(`sinon-chai`);
const Grade = require(`../../models/Grade`);
const { expect } = require(`chai`);
const sinon = require(`sinon`);
const objectWithout = require(`../../utils/objectWithout`);
const mongoose = require(`mongoose`);

chai.use(sinonChai);

describe(`The Grade Model`, () => {
  let object = {};

  beforeEach(async () => {
    object = {
      prelim: 95,
      midterm: 96,
      preFinal: 97,
      final: 98,
      class: mongoose.Types.ObjectId(),
      student: mongoose.Types.ObjectId(),
    };
    sinon.useFakeTimers(new Date(2020, 0, 1));
  });

  afterEach(async () => {
    sinon.restore();
    await Grade.find({}).deleteMany();
  });

  after(async () => {
    await mongoose.connection.dropDatabase();
  });

  describe(`The prelim field`, () => {
    it(`should have a prelim field specified`, async () => {
      const grade = await Grade.create(objectWithout(object));
      expect(grade.prelim).to.exist;
      expect(grade.prelim).to.equal(object.prelim);
    });

    it(`should throw an error for values lower than 0`, async () => {
      await Grade.create(Object.assign({ prelim: -1 }, objectWithout(object, `prelim`)))
        .then(result => expect(result).to.equal(undefined))
        .catch(err => expect(err._message, `expected a Mongoose error message`).to.exist);
    });

    it(`should throw an error for values higher than 100`, async () => {
      await Grade.create(Object.assign({ prelim: 101 }, objectWithout(object, `prelim`)))
        .then(result => expect(result).to.equal(undefined))
        .catch(err => expect(err._message, `expected a Mongoose error message`).to.exist);
    });

    it(`should set field to 0 as default value`, async () => {
      const grade = await Grade.create(objectWithout(object, `prelim`));
      expect(grade.prelim).to.exist;
      expect(grade.prelim).to.equal(0);
    });
  });

  describe(`The midterm field`, () => {
    it(`should have a midterm field specified`, async () => {
      const grade = await Grade.create(objectWithout(object));
      expect(grade.midterm).to.exist;
      expect(grade.midterm).to.equal(object.midterm);
    });

    it(`should throw an error for values lower than 0`, async () => {
      await Grade.create(Object.assign({ midterm: -1 }, objectWithout(object, `midterm`)))
        .then(result => expect(result).to.equal(undefined))
        .catch(err => expect(err._message, `expected a Mongoose error message`).to.exist);
    });

    it(`should throw an error for values higher than 100`, async () => {
      await Grade.create(Object.assign({ midterm: 101 }, objectWithout(object, `midterm`)))
        .then(result => expect(result).to.equal(undefined))
        .catch(err => expect(err._message, `expected a Mongoose error message`).to.exist);
    });

    it(`should set field to 0 as default value`, async () => {
      const grade = await Grade.create(objectWithout(object, `midterm`));
      expect(grade.midterm).to.exist;
      expect(grade.midterm).to.equal(0);
    });
  });

  describe(`The preFinal field`, () => {
    it(`should have a preFinal field specified`, async () => {
      const grade = await Grade.create(objectWithout(object));
      expect(grade.preFinal).to.exist;
      expect(grade.preFinal).to.equal(object.preFinal);
    });

    it(`should throw an error for values lower than 0`, async () => {
      await Grade.create(Object.assign({ preFinal: -1 }, objectWithout(object, `preFinal`)))
        .then(result => expect(result).to.equal(undefined))
        .catch(err => expect(err._message, `expected a Mongoose error message`).to.exist);
    });

    it(`should throw an error for values higher than 100`, async () => {
      await Grade.create(Object.assign({ preFinal: 101 }, objectWithout(object, `preFinal`)))
        .then(result => expect(result).to.equal(undefined))
        .catch(err => expect(err._message, `expected a Mongoose error message`).to.exist);
    });

    it(`should set field to 0 as default value`, async () => {
      const grade = await Grade.create(objectWithout(object, `preFinal`));
      expect(grade.preFinal).to.exist;
      expect(grade.preFinal).to.equal(0);
    });
  });

  describe(`The final field`, () => {
    it(`should have a final field specified`, async () => {
      const grade = await Grade.create(objectWithout(object));
      expect(grade.final).to.exist;
      expect(grade.final).to.equal(object.final);
    });

    it(`should throw an error for values lower than 0`, async () => {
      await Grade.create(Object.assign({ final: -1 }, objectWithout(object, `final`)))
        .then(result => expect(result).to.equal(undefined))
        .catch(err => expect(err._message, `expected a Mongoose error message`).to.exist);
    });

    it(`should throw an error for values higher than 100`, async () => {
      await Grade.create(Object.assign({ final: 101 }, objectWithout(object, `final`)))
        .then(result => expect(result).to.equal(undefined))
        .catch(err => expect(err._message, `expected a Mongoose error message`).to.exist);
    });

    it(`should set field to 0 as default value`, async () => {
      const grade = await Grade.create(objectWithout(object, `final`));
      expect(grade.final).to.exist;
      expect(grade.final).to.equal(0);
    });
  });

  describe(`The total field`, () => {
    it(`should have a total field`, async () => {
      const grade = await Grade.create(objectWithout(object));
      expect(grade.total).to.exist;
    });

    it(`should be equal to the computation of prelim, midterm, preFinal, and final grades`, async () => {
      const grade = await Grade.create(objectWithout(object));
      expect(grade.total).to.be.equal(
        (
          (object.prelim * 2) / 10 +
          (object.midterm * 2) / 10 +
          (object.preFinal * 2) / 10 +
          (object.final * 4) / 10
        ).toFixed(2),
      );
    });
  });

  describe(`The status field`, () => {
    it(`should have a status field`, async () => {
      const grade = await Grade.create(objectWithout(object));
      expect(grade.status).to.exist;
    });

    it(`should set a default value when status field is not set`, async () => {
      const grade = await Grade.create(objectWithout(object));
      expect(grade.status).to.equal(`Pending`);
    });

    it(`should throw an error when setting status to unauthorized value`, async () => {
      await Grade.create(Object.assign({ status: `Lul` }, objectWithout(object)))
        .then(result => expect(result).to.equal(undefined))
        .catch(err => expect(err._message, `expected a Mongoose error message`).to.exist);
    });
  });

  describe(`The class field`, () => {
    it(`should have an class field`, async () => {
      const grade = await Grade.create(objectWithout(object));
      expect(grade.class).to.exist;
      expect(grade.class).to.be.an.instanceOf(mongoose.Types.ObjectId);
    });

    it(`should throw an error if class field is missing`, async () => {
      await Grade.create(objectWithout(object, `class`))
        .then(result => expect(result).to.equal(undefined))
        .catch(err => expect(err._message, `expected a Mongoose error message`).to.exist);
    });
  });

  describe(`The student field`, () => {
    it(`should have an student field`, async () => {
      const grade = await Grade.create(objectWithout(object));
      expect(grade.student).to.exist;
      expect(grade.student).to.be.an.instanceOf(mongoose.Types.ObjectId);
    });

    it(`should throw an error if student field is missing`, async () => {
      await Grade.create(objectWithout(object, `student`))
        .then(result => expect(result).to.equal(undefined))
        .catch(err => expect(err._message, `expected a Mongoose error message`).to.exist);
    });
  });
});
