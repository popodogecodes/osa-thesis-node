const chai = require(`chai`);
const sinonChai = require(`sinon-chai`);
const Student = require(`../../models/Student`);
const { expect } = require(`chai`);
const sinon = require(`sinon`);
const objectWithout = require(`../../utils/objectWithout`);
const mongoose = require(`mongoose`);
const bcrypt = require(`bcryptjs`);

chai.use(sinonChai);

describe(`The Student Model`, () => {
  let object = {};

  beforeEach(async () => {
    object = {
      email: `popo@stronk.com`,
      password: `test password`,
      firstName: `Test`,
      middleName: `Ze`,
      lastName: `Popo`,
      birthday: new Date(1992, 7, 13),
      gender: `male`,
      houseNumber: `#101`,
      street: `Bonifacio`,
      barangay: `Poblacion`,
      city: `Gapan`,
      province: `Nueva Ecija`,
      civilStatus: `Married`,
      curriculum: mongoose.Types.ObjectId(),
      classes: [mongoose.Types.ObjectId()],
      credentials: { schoolName: `Awesum Skool`, yearGraduated: 2000, educationalLevel: `Awsum` },
    };
    sinon.useFakeTimers(new Date(2020, 0, 1));
  });

  afterEach(async () => {
    sinon.restore();
    await Student.find({}).deleteMany();
  });

  after(async () => {
    await mongoose.connection.dropDatabase();
  });

  describe(`The name field`, () => {
    it(`should have a name`, async () => {
      const student = await Student.create(objectWithout(object));
      expect(student.fullName).to.exist;
      expect(student.fullName).to.equal(`Popo, Test Ze`);
    });

    it(`should trim whitespaces around the name before saving`, async () => {
      const student = await Student.create(
        Object.assign(
          {
            firstName: `   Test`,
            middleName: `Ze      `,
            lastName: `   Popo      `,
          },
          objectWithout(object, `firstName`, `middleName`, `lastName`),
        ),
      );

      expect(student.fullName).to.equal(`Popo, Test Ze`);
    });

    it(`should throw an error when no name is provided`, async () => {
      await Student.create(objectWithout(object, `firstName`, `middleName`, `lastName`))
        .then(result => expect(result).to.equal(undefined))
        .catch(err => expect(err._message, `expected a Mongoose error message`).to.exist);
    });

    it(`should properly capitalize names provided`, async () => {
      const student = await Student.create(
        Object.assign(
          {
            firstName: `tEST`,
            middleName: `ze`,
            lastName: `PopO`,
          },
          objectWithout(object, `firstName`, `middleName`, `lastName`),
        ),
      );
      expect(student.fullName).to.exist;
      expect(student.fullName).to.equal(`Popo, Test Ze`);
    });
  });

  describe(`The birthday field`, () => {
    it(`should have a birthday`, async () => {
      const birthday = new Date(1992, 7, 13);
      const student = await Student.create(objectWithout(object));
      expect(student.birthday).to.exist;
      expect(student.birthday.getTime()).to.equal(birthday.getTime());
    });

    it(`should throw an error when no birthday is provided`, async () => {
      await Student.create(objectWithout(object, `birthday`))
        .then(result => expect(result).to.equal(undefined))
        .catch(err => expect(err._message, `expected a Mongoose error message`).to.exist);
    });

    it(`should throw an error when birthday's age is less than 13`, async () => {
      await Student.create(Object.assign({ birthday: new Date(2008, 0, 1) }, objectWithout(object, `birthday`)))
        .then(result => expect(result).to.equal(undefined))
        .catch(err => expect(err._message, `expected a Mongoose error message`).to.exist);
    });

    describe(`The age virtual field`, () => {
      it(`should have an age`, async () => {
        const age = 27;
        const student = await Student.create(objectWithout(object));
        expect(student.age).to.exist;
        expect(student.age).to.equal(age);
      });
    });
  });

  describe(`The gender field`, () => {
    it(`should have a gender`, async () => {
      const student = await Student.create(objectWithout(object));
      expect(student.gender).to.exist;
      expect(student.gender).to.equal(`male`);
    });

    it(`should lowercase submitted gender`, async () => {
      const student = await Student.create(Object.assign({ gender: `mALe` }, objectWithout(object)));
      expect(student.gender).to.exist;
      expect(student.gender).to.equal(`male`);
    });

    it(`should throw an error when gender is not provided`, async () => {
      await Student.create(objectWithout(object, `gender`))
        .then(result => expect(result).to.equal(undefined))
        .catch(err => expect(err._message, `expected a Mongoose error message`).to.exist);
    });

    it(`should throw an error when gender isn't male or female`, async () => {
      await Student.create(Object.assign({ gender: `luL` }, objectWithout(object, `gender`)))
        .then(result => expect(result).to.equal(undefined))
        .catch(err => expect(err._message, `expected a Mongoose error message`).to.exist);
    });
  });

  describe(`The address field`, () => {
    it(`should have an address`, async () => {
      const student = await Student.create(objectWithout(object));
      expect(student.address).to.exist;
      expect(student.address).to.equal(`#101 Bonifacio St., Brgy. Poblacion, Gapan, Nueva Ecija`);
    });

    it(`should throw an error when any of the address parts are missing`, async () => {
      await Student.create(objectWithout(object, `street`))
        .then(result => expect(result).to.equal(undefined))
        .catch(err => expect(err._message, `expected a Mongoose error message`).to.exist);
    });
  });

  describe(`The civil status field`, () => {
    it(`should have a civil status field`, async () => {
      const student = await Student.create(objectWithout(object));
      expect(student.civilStatus).to.exist;
      expect(student.civilStatus).to.equal(`Married`);
    });

    it(`should throw an error when no civil status is provided`, async () => {
      await Student.create(objectWithout(object, `civilStatus`))
        .then(result => expect(result).to.equal(undefined))
        .catch(err => expect(err._message, `expected a Mongoose error message`).to.exist);
    });

    it(`should throw an error when the civil status set isn't Single or Married`, async () => {
      await Student.create(Object.assign({ civilStatus: `Divorced` }, objectWithout(object, `civilStatus`)))
        .then(result => expect(result).to.equal(undefined))
        .catch(err => expect(err._message, `expected a Mongoose error message`).to.exist);
    });
  });

  describe(`The Curriculum Field`, () => {
    it(`should have a curriculum field specified`, async () => {
      const student = await Student.create(objectWithout(object));
      expect(student.curriculum).to.exist;
      expect(student.curriculum).to.be.an.instanceOf(mongoose.Types.ObjectId);
    });

    it(`should throw an error when curriculum field is not set`, async () => {
      await Student.create(objectWithout(object, `curriculum`))
        .then(result => expect(result).to.equal(undefined))
        .catch(err => expect(err._message, `expected a Mongoose error message`).to.exist);
    });
  });

  describe(`The Classes Field`, () => {
    it(`should have a classes field in schema`, async () => {
      const student = await Student.create(objectWithout(object));
      expect(student.classes).to.exist;
      expect(student.classes[0]).to.be.an.instanceOf(mongoose.Types.ObjectId);
    });
  });

  describe(`The email Field`, () => {
    it(`should have an email`, async () => {
      const student = await Student.create(objectWithout(object));
      expect(student.email).to.exist;
      expect(student.email).to.equal(object.email);
    });

    it(`must be a valid email address`, async () => {
      await Student.create(Object.assign({ email: `dudu` }, objectWithout(object, `email`)))
        .then(result => expect(result).to.equal(undefined))
        .catch(err => expect(err._message, `expected a Mongoose error message`).to.exist);
    });

    it(`should throw an error when email is missing`, async () => {
      await Student.create(objectWithout(object, `email`))
        .then(result => expect(result).to.equal(undefined))
        .catch(err => expect(err._message, `expected a Mongoose error message`).to.exist);
    });

    it(`should throw an error when email is not unique`, async () => {
      await Student.create(objectWithout(object));
      await Student.create(objectWithout(object))
        .then(result => expect(result).to.equal(undefined))
        .catch(err => expect(err._message, `expected a Mongoose error message`).to.exist);
    });
  });

  describe(`The password Field`, () => {
    it(`should have a password field`, async () => {
      const student = await Student.create(objectWithout(object));
      expect(student.password).to.exist;
    });

    it(`should have a default password when password is missing`, async () => {
      const student = await Student.create(objectWithout(object, `password`));
      expect(student.password).to.exist;
      expect(bcrypt.compareSync(`this is a temporary password`, student.password)).to.be.true;
    });

    it(`should hash the password before saving to the database`, async () => {
      const student = await Student.create(objectWithout(object));
      expect(bcrypt.compareSync(object.password, student.password)).to.be.true;
    });

    it(`should not rehash the password if it was not modified`, async () => {
      const student = await Student.create(objectWithout(object));
      student.email = `changed@stronk.com`;
      const reSavedStudent = await student.save();
      expect(bcrypt.compareSync(object.password, reSavedStudent.password)).to.be.true;
    });
  });

  describe(`The credentials field`, () => {
    it(`should have a credentials field`, async () => {
      const student = await Student.create(objectWithout(object));
      expect(student.credentials[0]).to.exist;
      expect(student.credentials[0]).to.include(object.credentials);
    });

    it(`should throw an error if any detail is missing from the object`, async () => {
      await Student.create(
        Object.assign({ credentials: { schoolName: `Awesum Skool` } }, objectWithout(object, `credentials`)),
      )
        .then(result => expect(result).to.equal(undefined))
        .catch(err => expect(err._message, `expected a Mongoose error message`).to.exist);
    });
  });
});
