const chai = require(`chai`);
const sinonChai = require(`sinon-chai`);
const Semester = require(`../../models/Semester`);
const { expect } = require(`chai`);
const sinon = require(`sinon`);
const objectWithout = require(`../../utils/objectWithout`);
const mongoose = require(`mongoose`);

chai.use(sinonChai);

describe(`The semester Model`, () => {
  let object = {};

  beforeEach(async () => {
    object = {
      startDate: new Date(2021, 6, 1),
      endDate: new Date(2021, 10, 28),
      schoolYear: 2021,
      instance: 1,
      classes: [mongoose.Types.ObjectId(), mongoose.Types.ObjectId()],
    };
    sinon.useFakeTimers(new Date(2020, 0, 1));
  });

  afterEach(async () => {
    sinon.restore();
    await Semester.find({}).deleteMany();
  });

  after(async () => {
    await mongoose.connection.dropDatabase();
  });

  describe(`The start date field`, () => {
    it(`Should have a Start date specified`, async () => {
      const semester = await Semester.create(objectWithout(object));
      expect(semester.startDate).to.exist;
      expect(semester.startDate.getTime()).to.equal(object.startDate.getTime());
    });

    it(`should throw an error when no Start date is specified`, async () => {
      await Semester.create(objectWithout(object, `startDate`))
        .then(result => expect(result).to.equal(undefined))
        .catch(err => expect(err._message, `expected a Mongoose error message`).to.exist);
    });
  });

  describe(`The end date field`, () => {
    it(`Should have a Start date specified`, async () => {
      const semester = await Semester.create(objectWithout(object));
      expect(semester.endDate).to.exist;
      expect(semester.endDate.getTime()).to.equal(object.endDate.getTime());
    });

    it(`should throw an error when no Start date is specified`, async () => {
      await Semester.create(objectWithout(object, `endDate`))
        .then(result => expect(result).to.equal(undefined))
        .catch(err => expect(err._message, `expected a Mongoose error message`).to.exist);
    });
  });

  describe(`The School Year field`, () => {
    it(`Should have a school year specified`, async () => {
      const semester = await Semester.create(objectWithout(object));
      expect(semester.schoolYear).to.exist;
    });

    it(`should throw an error when no school year is specified`, async () => {
      await Semester.create(objectWithout(object, `endDate`))
        .then(result => expect(result).to.equal(undefined))
        .catch(err => expect(err._message, `expected a Mongoose error message`).to.exist);
    });

    it(`should format the school year field when being retrieved`, async () => {
      const semester = await Semester.create(objectWithout(object));
      expect(semester.schoolYear).to.not.equal(object.schoolYear);
      expect(semester.schoolYear).to.equal(`2021 - 2022`);
    });
  });

  describe(`The instance field`, () => {
    it(`Should have an instance field specified`, async () => {
      const semester = await Semester.create(objectWithout(object));
      expect(semester.instance).to.exist;
    });

    it(`should throw an error when no instance is specified`, async () => {
      await Semester.create(objectWithout(object, `instance`))
        .then(result => expect(result).to.equal(undefined))
        .catch(err => expect(err._message, `expected a Mongoose error message`).to.exist);
    });

    it(`should format the instance field when being retrieved`, async () => {
      const semester = await Semester.create(objectWithout(object));
      expect(semester.instance).to.not.equal(object.instance);
      expect(semester.instance).to.equal(`Semester 1`);
    });
  });

  describe(`The Classes Field`, () => {
    it(`should have a classes field in schema`, async () => {
      const semester = await Semester.create(objectWithout(object));
      expect(semester.classes).to.exist;
      expect(semester.classes[0]).to.be.an.instanceOf(mongoose.Types.ObjectId);
    });
  });
});
