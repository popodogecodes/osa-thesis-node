const chai = require(`chai`);
const sinonChai = require(`sinon-chai`);
const Classes = require(`../../models/Class`);
const Semester = require(`../../models/Semester`);
const { expect } = require(`chai`);
const sinon = require(`sinon`);
const objectWithout = require(`../../utils/objectWithout`);
const mongoose = require(`mongoose`);

chai.use(sinonChai);

describe(`The Class Model`, () => {
  let object = {};
  let semester = {};
  const semesterId = mongoose.Types.ObjectId();

  beforeEach(async () => {
    object = {
      professor: mongoose.Types.ObjectId(),
      subject: mongoose.Types.ObjectId(),
      schedule: {
        lecture: [{ time: `15-16`, room: `rm302`, day: `Tuesday` }, { time: `11-12`, room: `rm302`, day: `Thursday` }],
        laboratory: [{ time: `9-12`, room: `lab2`, day: `wednesday` }],
      },
      attendees: [
        { student: mongoose.Types.ObjectId(), grade: mongoose.Types.ObjectId() },
        { student: mongoose.Types.ObjectId(), grade: mongoose.Types.ObjectId() },
        { student: mongoose.Types.ObjectId(), grade: mongoose.Types.ObjectId() },
      ],
      semester: semesterId,
    };
    semester = {
      _id: semesterId,
      startDate: new Date(2021, 6, 1),
      endDate: new Date(2021, 10, 28),
      schoolYear: 2021,
      instance: 1,
      classes: [],
    };
    sinon.useFakeTimers(new Date(2020, 0, 1));
  });

  afterEach(async () => {
    sinon.restore();
    await Classes.find({}).deleteMany();
    await Semester.find({}).deleteMany();
  });

  after(async () => {
    await mongoose.connection.dropDatabase();
  });

  describe(`The professor field`, () => {
    it(`should have a professor field specified`, async () => {
      await Semester.create(objectWithout(semester));
      const classes = await Classes.create(objectWithout(object));
      expect(classes.professor).to.exist;
      expect(classes.professor).to.equal(object.professor);
    });

    it(`should throw an error when professor field is missing`, async () => {
      await Semester.create(objectWithout(semester));
      await Classes.create(objectWithout(object, `professor`))
        .then(result => expect(result).to.equal(undefined))
        .catch(err => expect(err._message, `expected a Mongoose error message`).to.exist);
    });
  });

  describe(`The Subject field`, () => {
    it(`should have a subject field specified`, async () => {
      await Semester.create(objectWithout(semester));
      const classes = await Classes.create(objectWithout(object));
      expect(classes.subject).to.exist;
      expect(classes.subject).to.equal(object.subject);
    });

    it(`should throw an error when subject field is missing`, async () => {
      await Semester.create(objectWithout(semester));
      await Classes.create(objectWithout(object, `subject`))
        .then(result => expect(result).to.equal(undefined))
        .catch(err => expect(err._message, `expected a Mongoose error message`).to.exist);
    });
  });

  describe(`The Attendees field`, () => {
    it(`should have an attendees field specified`, async () => {
      await Semester.create(objectWithout(semester));
      const classes = await Classes.create(objectWithout(object));
      expect(classes.attendees).to.exist;
    });

    it(`should throw an error when the attendees sub fields are incomplete`, async () => {
      await Semester.create(objectWithout(semester));
      await Classes.create(
        Object.assign({ attendees: { student: mongoose.Types.ObjectId() } }, objectWithout(object, `attendees`)),
      )
        .then(result => expect(result).to.equal(undefined))
        .catch(err => expect(err._message, `expected a Mongoose error message`).to.exist);
    });
  });

  describe(`Schedule field`, () => {
    it(`should have a Schedule object specified`, async () => {
      await Semester.create(objectWithout(semester));
      const classes = await Classes.create(objectWithout(object));
      expect(classes.schedule).to.exist;
    });

    it(`should throw an error when schedule sub fields are incomplete`, async () => {
      await Semester.create(objectWithout(semester));
      await Classes.create(
        Object.assign({ schedule: { lecture: { time: `15-16` } } }, objectWithout(object, `schedule`)),
      )
        .then(result => expect(result).to.equal(undefined))
        .catch(err => expect(err._message, `expected a Mongoose error message`).to.exist);
    });
  });

  describe(`Semester field`, () => {
    it(`should have a semester field specified`, async () => {
      await Semester.create(objectWithout(semester));
      const classes = await Classes.create(objectWithout(object));
      expect(classes.semester).to.exist;
      expect(classes.semester).to.be.an.instanceOf(mongoose.Types.ObjectId);
    });

    it(`should throw an error when schedule sub fields are incomplete`, async () => {
      await Semester.create(objectWithout(semester));
      await Classes.create(objectWithout(object, `semester`))
        .then(result => expect(result).to.equal(undefined))
        .catch(err => expect(err._message, `expected a Mongoose error message`).to.exist);
    });
  });
});
