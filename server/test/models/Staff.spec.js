const chai = require(`chai`);
const sinonChai = require(`sinon-chai`);
const Staff = require(`../../models/Staff`);
const { expect } = require(`chai`);
const sinon = require(`sinon`);
const objectWithout = require(`../../utils/objectWithout`);
const bcrypt = require(`bcryptjs`);
const mongoose = require(`mongoose`);

chai.use(sinonChai);

describe(`The Staff Model`, () => {
  let object = {};

  beforeEach(async () => {
    object = {
      email: `popo@stronk.com`,
      password: `test password`,
      firstName: `Test`,
      middleName: `Ze`,
      lastName: `Popo`,
      birthday: new Date(1992, 7, 13),
      gender: `male`,
      houseNumber: `#101`,
      street: `Bonifacio`,
      barangay: `Poblacion`,
      city: `Gapan`,
      province: `Nueva Ecija`,
      civilStatus: `Married`,
      credentials: { schoolName: `Awesum Skool`, yearGraduated: 2000, educationalLevel: `Awsum` },
      role: `Janitor`,
    };
    sinon.useFakeTimers(new Date(2020, 0, 1));
  });

  afterEach(async () => {
    sinon.restore();
    await Staff.find({}).deleteMany();
  });

  after(async () => {
    await mongoose.connection.dropDatabase();
  });

  describe(`The name field`, () => {
    it(`should have a name`, async () => {
      const staff = await Staff.create(objectWithout(object));
      expect(staff.fullName).to.exist;
      expect(staff.fullName).to.equal(`Popo, Test Ze`);
    });

    it(`should trim whitespaces around the name before saving`, async () => {
      const staff = await Staff.create(
        Object.assign(
          { firstName: `   Test`, middleName: `Ze      `, lastName: `   Popo      ` },
          objectWithout(object, `firstName`, `middleName`, `lastName`),
        ),
      );

      expect(staff.fullName).to.equal(`Popo, Test Ze`);
    });

    it(`should throw an error when no name is provided`, async () => {
      await Staff.create(objectWithout(object, `firstName`, `middleName`, `lastName`))
        .then(result => expect(result).to.equal(undefined))
        .catch(err => expect(err._message, `expected a Mongoose error message`).to.exist);
    });

    it(`should properly capitalize names provided`, async () => {
      const staff = await Staff.create(
        Object.assign(
          { firstName: `tEST`, middleName: `ze`, lastName: `PopO` },
          objectWithout(object, `firstName`, `middleName`, `lastName`),
        ),
      );
      expect(staff.fullName).to.exist;
      expect(staff.fullName).to.equal(`Popo, Test Ze`);
    });
  });

  describe(`The birthday field`, () => {
    it(`should have a birthday`, async () => {
      const birthday = new Date(1992, 7, 13);
      const staff = await Staff.create(objectWithout(object));
      expect(staff.birthday).to.exist;
      expect(staff.birthday.getTime()).to.equal(birthday.getTime());
    });

    it(`should throw an error when no birthday is provided`, async () => {
      await Staff.create(objectWithout(object, `birthday`))
        .then(result => expect(result).to.equal(undefined))
        .catch(err => expect(err._message, `expected a Mongoose error message`).to.exist);
    });

    it(`should throw an error when birthday's age is less than 13`, async () => {
      await Staff.create(Object.assign({ birthday: new Date(2008, 0, 1) }, objectWithout(object, `birthday`)))
        .then(result => expect(result).to.equal(undefined))
        .catch(err => expect(err._message, `expected a Mongoose error message`).to.exist);
    });

    describe(`The age virtual field`, () => {
      it(`should have an age`, async () => {
        const age = 27;
        const staff = await Staff.create(objectWithout(object));
        expect(staff.age).to.exist;
        expect(staff.age).to.equal(age);
      });
    });
  });

  describe(`The gender field`, () => {
    it(`should have a gender`, async () => {
      const staff = await Staff.create(objectWithout(object));
      expect(staff.gender).to.exist;
      expect(staff.gender).to.equal(`male`);
    });

    it(`should lowercase submitted gender`, async () => {
      const staff = await Staff.create(Object.assign({ gender: `mALe` }, objectWithout(object)));
      expect(staff.gender).to.exist;
      expect(staff.gender).to.equal(`male`);
    });

    it(`should throw an error when gender is not provided`, async () => {
      await Staff.create(objectWithout(object, `gender`))
        .then(result => expect(result).to.equal(undefined))
        .catch(err => expect(err._message, `expected a Mongoose error message`).to.exist);
    });

    it(`should throw an error when gender isn't male or female`, async () => {
      await Staff.create(Object.assign({ gender: `luL` }, objectWithout(object, `gender`)))
        .then(result => expect(result).to.equal(undefined))
        .catch(err => expect(err._message, `expected a Mongoose error message`).to.exist);
    });
  });

  describe(`The address field`, () => {
    it(`should have an address`, async () => {
      const staff = await Staff.create(objectWithout(object));
      expect(staff.address).to.exist;
      expect(staff.address).to.equal(`#101 Bonifacio St., Brgy. Poblacion, Gapan, Nueva Ecija`);
    });

    it(`should throw an error when any of the address parts are missing`, async () => {
      await Staff.create(objectWithout(object, `street`))
        .then(result => expect(result).to.equal(undefined))
        .catch(err => expect(err._message, `expected a Mongoose error message`).to.exist);
    });
  });

  describe(`The civil status field`, () => {
    it(`should have a civil status field`, async () => {
      const staff = await Staff.create(objectWithout(object));
      expect(staff.civilStatus).to.exist;
      expect(staff.civilStatus).to.equal(`Married`);
    });

    it(`should throw an error when no civil status is provided`, async () => {
      await Staff.create(objectWithout(object, `civilStatus`))
        .then(result => expect(result).to.equal(undefined))
        .catch(err => expect(err._message, `expected a Mongoose error message`).to.exist);
    });

    it(`should throw an error when the civil status set isn't Single or Married`, async () => {
      await Staff.create(Object.assign({ civilStatus: `Divorced` }, objectWithout(object, `civilStatus`)))
        .then(result => expect(result).to.equal(undefined))
        .catch(err => expect(err._message, `expected a Mongoose error message`).to.exist);
    });
  });

  describe(`The email Field`, () => {
    it(`should have an email`, async () => {
      const staff = await Staff.create(objectWithout(object));
      expect(staff.email).to.exist;
      expect(staff.email).to.equal(object.email);
    });

    it(`must be a valid email address`, async () => {
      await Staff.create(Object.assign({ email: `dudu` }, objectWithout(object, `email`)))
        .then(result => expect(result).to.equal(undefined))
        .catch(err => expect(err._message, `expected a Mongoose error message`).to.exist);
    });

    it(`should throw an error when email is missing`, async () => {
      await Staff.create(objectWithout(object, `email`))
        .then(result => expect(result).to.equal(undefined))
        .catch(err => expect(err._message, `expected a Mongoose error message`).to.exist);
    });

    it(`should throw an error when email is not unique`, async () => {
      await Staff.create(objectWithout(object));
      await Staff.create(objectWithout(object))
        .then(result => expect(result).to.equal(undefined))
        .catch(err => expect(err._message, `expected a Mongoose error message`).to.exist);
    });
  });

  describe(`The password Field`, () => {
    it(`should have a password field`, async () => {
      const staff = await Staff.create(objectWithout(object));
      expect(staff.password).to.exist;
    });

    it(`should have a default password when password is missing`, async () => {
      const staff = await Staff.create(objectWithout(object, `password`));
      expect(staff.password).to.exist;
      expect(bcrypt.compareSync(`this is a temporary password`, staff.password)).to.be.true;
    });

    it(`should hash the password before saving to the database`, async () => {
      const staff = await Staff.create(objectWithout(object));
      expect(bcrypt.compareSync(object.password, staff.password)).to.be.true;
    });

    it(`should not rehash the password if it was not modified`, async () => {
      const staff = await Staff.create(objectWithout(object));
      staff.email = `changed@stronk.com`;
      const reSavedStaff = await staff.save();
      expect(bcrypt.compareSync(object.password, reSavedStaff.password)).to.be.true;
    });
  });

  describe(`The credentials field`, () => {
    it(`should have a credentials field`, async () => {
      const staff = await Staff.create(objectWithout(object));
      expect(staff.credentials[0]).to.exist;
      expect(staff.credentials[0]).to.include(object.credentials);
    });

    it(`should throw an error if any detail is missing from the object`, async () => {
      await Staff.create(
        Object.assign({ credentials: { schoolName: `Awesum Skool` } }, objectWithout(object, `credentials`)),
      )
        .then(result => expect(result).to.equal(undefined))
        .catch(err => expect(err._message, `expected a Mongoose error message`).to.exist);
    });
  });

  describe(`The role field`, () => {
    it(`should have a role field`, async () => {
      const staff = await Staff.create(objectWithout(object));
      expect(staff.role).to.exist;
      expect(staff.role).to.equal(`Janitor`);
    });

    it(`should throw an error if role field is missing`, async () => {
      await Staff.create(objectWithout(object, `role`))
        .then(result => expect(result).to.equal(undefined))
        .catch(err => expect(err._message, `expected a Mongoose error message`).to.exist);
    });
  });
});
