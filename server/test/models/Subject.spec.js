const chai = require(`chai`);
const sinonChai = require(`sinon-chai`);
const Subject = require(`../../models/Subject`);
const { expect } = require(`chai`);
const sinon = require(`sinon`);
const objectWithout = require(`../../utils/objectWithout`);
const mongoose = require(`mongoose`);

chai.use(sinonChai);

describe(`The Subject model`, () => {
  let object = {};

  beforeEach(async () => {
    object = {
      name: `Ultra Algebra`,
      classCode: `CS102`,
      units: { lecture: 3, laboratory: 0 },
      isCountedGPA: true,
      prerequisites: { subjects: [mongoose.Types.ObjectId(), mongoose.Types.ObjectId()], yearLevel: 1 },
    };
    sinon.useFakeTimers(new Date(2020, 0, 1));
  });

  afterEach(async () => {
    sinon.restore();
    await Subject.find({}).deleteMany();
  });

  after(async () => {
    await mongoose.connection.dropDatabase();
  });

  describe(`The name field`, () => {
    it(`should have a name field specified`, async () => {
      const subject = await Subject.create(objectWithout(object));
      expect(subject.name).to.exist;
      expect(subject.name).to.equal(object.name);
    });

    it(`should throw an error when there is no given name`, async () => {
      await Subject.create(objectWithout(object, `name`))
        .then(result => expect(result).to.equal(undefined))
        .catch(err => expect(err._message, `expected a Mongoose error message`).to.exist);
    });
  });

  describe(`The class code field`, () => {
    it(`should have a class code field specified`, async () => {
      const subject = await Subject.create(objectWithout(object));
      expect(subject.classCode).to.exist;
      expect(subject.classCode).to.equal(object.classCode);
    });

    it(`should throw an error when there is no given class code`, async () => {
      await Subject.create(objectWithout(object, `classCode`))
        .then(result => expect(result).to.equal(undefined))
        .catch(err => expect(err._message, `expected a Mongoose error message`).to.exist);
    });

    it(`should uppercase the class code field before saving`, async () => {
      const subject = await Subject.create(Object.assign({ classCode: `cs102` }, objectWithout(object, `classCode`)));
      expect(subject.classCode).to.exist;
      expect(subject.classCode).to.equal(`CS102`);
    });
  });

  describe(`the GPA counted field`, () => {
    it(`should have a GPA counted field specified`, async () => {
      const subject = await Subject.create(objectWithout(object));
      expect(subject.isCountedGPA).to.exist;
      expect(subject.isCountedGPA).to.equal(object.isCountedGPA);
    });

    it(`should throw an error when there is no given class code`, async () => {
      await Subject.create(objectWithout(object, `isCountedGPA`))
        .then(result => expect(result).to.equal(undefined))
        .catch(err => expect(err._message, `expected a Mongoose error message`).to.exist);
    });
  });

  describe(`the Subject Units field`, () => {
    it(`should have a subject units object field`, async () => {
      const subject = await Subject.create(objectWithout(object));
      expect(subject.units).to.exist;
      expect(subject.units).to.include(object.units);
    });

    it(`should throw an error when there is no given lecture sub field`, async () => {
      await Subject.create(Object.assign({ units: { laboratory: 0 } }, objectWithout(object, `units`)))
        .then(result => expect(result).to.equal(undefined))
        .catch(err => expect(err._message, `expected a Mongoose error message`).to.exist);
    });

    it(`should fill the laboratory sub field with 0 by default when missing`, async () => {
      const subject = await Subject.create(Object.assign({ units: { lecture: 3 } }, objectWithout(object, `units`)));
      expect(subject.units).to.exist;
      expect(subject.units).to.include({ lecture: 3, laboratory: 0 });
    });

    it(`should throw an error when lecture sub field is below 1`, async () => {
      await Subject.create(Object.assign({ units: { lecture: 0 } }, objectWithout(object, `units`)))
        .then(result => expect(result).to.equal(undefined))
        .catch(err => expect(err._message, `expected a Mongoose error message`).to.exist);
    });
  });

  describe(`the Prerequisites field`, () => {
    it(`should have a subject units object field`, async () => {
      const subject = await Subject.create(objectWithout(object));
      expect(subject.prerequisites).to.exist;
      expect(subject.prerequisites).to.deep.include(object.prerequisites);
    });

    it(`should throw an error when year level sub field is below 1`, async () => {
      await Subject.create(Object.assign({ prerequisites: { yearLevel: 0 } }, objectWithout(object, `prerequisites`)))
        .then(result => expect(result).to.equal(undefined))
        .catch(err => expect(err._message, `expected a Mongoose error message`).to.exist);
    });
  });
});
