const chai = require(`chai`);
const sinonChai = require(`sinon-chai`);
const Curriculum = require(`../../models/Curriculum`);
const { expect } = require(`chai`);
const sinon = require(`sinon`);
const objectWithout = require(`../../utils/objectWithout`);
const mongoose = require(`mongoose`);

chai.use(sinonChai);

describe(`The Curriculum model`, () => {
  let object = {};

  beforeEach(async () => {
    object = { year: 2014, course: `BSIT`, subjects: [[mongoose.Types.ObjectId(), mongoose.Types.ObjectId()]] };
    sinon.useFakeTimers(new Date(2020, 0, 1));
  });

  afterEach(async () => {
    sinon.restore();
    await Curriculum.find({}).deleteMany();
  });

  after(async () => {
    await mongoose.connection.dropDatabase();
  });

  describe(`The year field`, () => {
    it(`should have a year field`, async () => {
      const curriculum = await Curriculum.create(objectWithout(object));
      expect(curriculum.year).to.exist;
      expect(curriculum.year).to.equal(object.year);
    });

    it(`should throw an error if there is no year field present`, async () => {
      await Curriculum.create(objectWithout(object, `year`))
        .then(result => expect(result).to.equal(undefined))
        .catch(err => expect(err._message, `expected a Mongoose error message`).to.exist);
    });
  });

  describe(`The course field`, () => {
    it(`should have a course field`, async () => {
      const curriculum = await Curriculum.create(objectWithout(object));
      expect(curriculum.course).to.exist;
      expect(curriculum.course).to.equal(object.course);
    });

    it(`should uppercase the course field before saving`, async () => {
      const curriculum = await Curriculum.create(Object.assign({ course: `bsit` }, objectWithout(object, `course`)));
      expect(curriculum.course).to.exist;
      expect(curriculum.course).to.equal(`BSIT`);
    });

    it(`should throw an error if there is no course field present`, async () => {
      await Curriculum.create(objectWithout(object, `year`))
        .then(result => expect(result).to.equal(undefined))
        .catch(err => expect(err._message, `expected a Mongoose error message`).to.exist);
    });
  });

  describe(`The subjects field`, () => {
    it(`should have a subjects field`, async () => {
      const curriculum = await Curriculum.create(objectWithout(object));
      expect(curriculum.subjects[0]).to.exist;
      expect(curriculum.subjects[0]).to.deep.equal(object.subjects[0]);
    });
  });
});
