const { expect } = require(`chai`);
const httpMocks = require(`node-mocks-http`);
const controller = require(`../../controllers/home`);

describe(`The homepage route`, () => {
  it(`should return 'Welcome to the Front Page!' when called`, async () => {
    const request = httpMocks.createRequest({
      method: `GET`,
      path: `/`,
    });

    const response = httpMocks.createResponse();

    controller(request, response);

    const data = response._getData();
    expect(data).to.equal(`Welcome to the Front Page!`);
  });
});
