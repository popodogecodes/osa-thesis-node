const mongoose = require(`mongoose`);

before(async () => {
  await mongoose.connect(`mongodb://localhost:27017/OSA_test`, {
    useCreateIndex: true,
    useNewUrlParser: true,
  });
});
