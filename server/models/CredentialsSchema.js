const mongoose = require(`mongoose`);

const titleCase = require(`../utils/firstLetterCapitalize`);

const CredentialsSchema = mongoose.Schema({
  schoolName: {
    type: String,
    required: true,
    set: value => titleCase(value),
  },

  yearGraduated: {
    type: Number,
    required: true,
    minLength: 4,
  },

  educationalLevel: {
    type: String,
    required: true,
    set: value => titleCase(value),
  },
});

module.exports = CredentialsSchema;
