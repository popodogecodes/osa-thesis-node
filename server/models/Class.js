const mongoose = require(`mongoose`);

const Professor = require(`./Teacher`);
const Grade = require(`./Grade`);
const Semester = require(`./Semester`);

const titleCase = require(`../utils/firstLetterCapitalize`);

const incrementRevision = require(`./schemaMethods/incrementRevision`);

const ClassSchema = mongoose.Schema({
  professor: {
    type: mongoose.Schema.Types.ObjectId,
    required: true,
  },

  subject: {
    type: mongoose.Schema.Types.ObjectId,
    required: true,
  },

  attendees: [
    {
      student: { type: mongoose.Schema.Types.ObjectId, required: true },
      grade: { type: mongoose.Schema.Types.ObjectId, required: true },
    },
  ],

  schedule: {
    lecture: [
      {
        time: { type: String, required: true, trim: true },
        day: { type: String, required: true, trim: true, set: value => titleCase(value) },
        room: { type: String, required: true, trim: true },
      },
    ],
    laboratory: [
      {
        time: { type: String, required: true, trim: true },
        day: { type: String, required: true, trim: true, set: value => titleCase(value) },
        room: { type: String, required: true, trim: true },
      },
    ],
  },

  semester: { type: mongoose.Schema.Types.ObjectId, required: true },
});

ClassSchema.pre(`save`, incrementRevision);

ClassSchema.pre(`save`, async function updateProfessorChanges() {
  if (!this.isNew && this.isModified(`professor`)) {
    const professor = await Professor.findById(this.professor);
    professor.classes.push(this.id);
    await professor.save();
  }
});

ClassSchema.pre(`save`, async function hookToSemester() {
  if (this.isNew) {
    const semester = await Semester.findById(this.semester);
    await semester.classes.push(this.id);
    await semester.save();
  }
});

ClassSchema.pre(`remove`, async function associationCleanup() {
  const Student = mongoose.model(`Student`);
  this.attendees.forEach(async attendee => {
    const student = await Student.findById(attendee.student);
    const index = student.classes.indexOf(this.id);
    student.classes.splice(index, 1);
    await student.save();
    await Grade.findByIdAndDelete(attendee.grade);
  });
  const semester = await Semester.findById(this.semester);
  let index = semester.classes.indexOf(this.id);
  semester.classes.splice(index, 1);
  await semester.save();
  const professor = await Professor.findById(this.professor);
  index = professor.classes.indexOf(this.id);
  professor.classes.splice(index, 1);
  await professor.save();
});

ClassSchema.methods.replaceProfessor = async function replaceProfessor(id) {
  const oldProfessor = await Professor.findById(this.professor);
  const newProfessor = id;
  const index = oldProfessor.classes.indexOf(this.id);
  oldProfessor.classes.splice(index, 1);
  this.professor = newProfessor;
  await oldProfessor.save();
  return this;
};

ClassSchema.methods.addStudent = async function addStudent(id) {
  const Student = mongoose.model(`Student`);
  const newStudent = await Student.findById(id);
  newStudent.addClass(this.id);
  const grade = await Grade.create({ student: newStudent, class: this.id });
  this.attendees.push({ grade, student: newStudent });
  await newStudent.save();
  return this;
};

ClassSchema.methods.removeStudent = async function removeStudent(id) {
  const Student = mongoose.model(`Student`);
  const removedStudent = await Student.findById(id);
  const index = removedStudent.classes.indexOf(this.id);
  removedStudent.classes.splice(index, 1);
  await removedStudent.save();
  this.attendees.forEach(async (attendee, attendeeIndex) => {
    if (attendee.student.equals(removedStudent.id)) {
      this.attendees.splice(attendeeIndex, 1);
      await Grade.findByIdAndDelete(attendee.grade);
    }
  });
  await this.save();
};

ClassSchema.set(`toJSON`, { virtuals: true });

module.exports = mongoose.model(`Class`, ClassSchema);
