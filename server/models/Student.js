const mongoose = require(`mongoose`);
const uniqueValidator = require(`mongoose-unique-validator`);
const validator = require(`mongoose-validator`);
const bcrypt = require(`bcryptjs`);
const _ = require(`lodash`);

const titleCase = require(`../utils/firstLetterCapitalize`);
const CredentialsSchema = require(`./CredentialsSchema`);

const Class = require(`./Class`);
const Grade = require(`./Grade`);
const Curriculum = require(`./Curriculum`);
const Subject = require(`./Subject`);
const File = require(`./Files`);

const generateHashedPassword = require(`./schemaMethods/generateHashedPassword`);
const generateDefaultPassword = require(`./schemaMethods/generateDefaultPassword`);
const incrementRevision = require(`./schemaMethods/incrementRevision`);
const rejectLowerThan13 = require(`./schemaMethods/rejectLowerThan13`);

const StudentSchema = mongoose.Schema({
  firstName: {
    type: String,
    required: [true, `First Name is required!`],
    trim: true,
    set: value => titleCase(value),
  },

  middleName: {
    type: String,
    required: [true, `Middle Name is required!`],
    trim: true,
    set: value => titleCase(value),
  },

  lastName: {
    type: String,
    required: [true, `Last Name is required!`],
    trim: true,
    set: value => titleCase(value),
  },

  birthday: {
    required: true,
    type: Date,
    validate: {
      validator: rejectLowerThan13,
      message: value => `${value} needs to be to have a computed age of 13 or bigger. `,
    },
  },

  gender: {
    required: true,
    type: String,
    enum: [`male`, `female`],
    lowercase: true,
    trim: true,
  },

  houseNumber: {
    required: true,
    type: String,
    trim: true,
  },

  street: {
    required: true,
    type: String,
    trim: true,
    set: value => titleCase(value),
  },

  barangay: {
    required: true,
    type: String,
    trim: true,
    set: value => titleCase(value),
  },

  city: {
    required: true,
    type: String,
    trim: true,
    set: value => titleCase(value),
  },

  province: {
    required: true,
    type: String,
    trim: true,
    set: value => titleCase(value),
  },

  civilStatus: {
    required: true,
    type: String,
    trim: true,
    enum: [`Single`, `Married`],
    set: value => titleCase(value),
  },

  curriculum: {
    required: true,
    type: mongoose.Schema.Types.ObjectId,
  },

  classes: [{ type: mongoose.Schema.Types.ObjectId }],

  email: {
    type: String,
    required: [true, `email is required!`],
    unique: true,
    trim: true,
    lowercase: true,
    validate: validator({ validator: `isEmail`, message: `must provide a valid email address!` }),
  },

  password: {
    type: String,
  },

  enrollmentStatus: {
    isCurrentlyEnrolled: { type: Boolean, default: false },
    hasPaidPrelims: { type: Boolean, default: false },
    hasPaidMidterms: { type: Boolean, default: false },
    hasPaidFinals: { type: Boolean, default: false },
  },

  credentials: [CredentialsSchema],

  yearLevel: { type: Number, default: 1 },

  isRegular: { type: Boolean, default: true },

  isInSection: { type: Boolean, default: false },

  form137A: { type: Boolean, default: false },
  form138: { type: Boolean, default: false },
  GMC: { type: Boolean, default: false },
  ALS: { type: Boolean, default: false },
  NCAEResult: { type: Boolean, default: false },
  BC: { type: Boolean, default: false },
  TOR: { type: Boolean, default: false },
  TCG: { type: Boolean, default: false },
  honorableDismissal: { type: Boolean, default: false },
  pictures: { type: Boolean, default: false },

  attachment: { type: mongoose.Schema.Types.ObjectId },
});

StudentSchema.pre(`save`, incrementRevision);
StudentSchema.pre(`save`, generateDefaultPassword);
StudentSchema.pre(`save`, generateHashedPassword);

StudentSchema.virtual(`fullName`).get(function getFullName() {
  return `${this.lastName}, ${this.firstName} ${this.middleName}`;
});

StudentSchema.virtual(`age`).get(function getAge() {
  const today = new Date();
  const birthDate = this.birthday;
  let age = today.getFullYear() - birthDate.getFullYear();
  const m = today.getMonth() - birthDate.getMonth();
  if (m < 0 || (m === 0 && today.getDate() < birthDate.getDate())) {
    age -= 1;
  }
  return age;
});

StudentSchema.virtual(`address`).get(function getAddress() {
  return `${this.houseNumber} ${this.street} St., Brgy. ${this.barangay}, ${this.city}, ${this.province}`;
});

StudentSchema.methods.validPassword = function comparePassword(password) {
  return bcrypt.compareSync(password, this.password);
};

StudentSchema.methods.addClass = async function addClass(id) {
  this.classes.push(id);
  this.save();
};

StudentSchema.methods.assessment = async function assessment() {
  const completedSubjects = [];
  const subjectsToBeRecommended = [];
  const subjectsForRegularStatus = [];
  let isRegular = false;
  let semesterWithUnfinishedSubjects = 0;
  for await (const id of this.classes) {
    const subject = await Class.findById(id);
    const grade = await Grade.findOne({ class: subject.id, student: this.id });
    if (grade.status === `Completed`) {
      completedSubjects.push(subject.subject.toString());
    }
  }

  const curriculum = await Curriculum.findById(this.curriculum);

  for (let semesterIndex = 0; semesterIndex < curriculum.subjects.length; semesterIndex += 1) {
    // for each semester in the curriculum
    const subjects = curriculum.subjects[semesterIndex].map(subject => subject.toString());
    const unfinishedSubjects = _.difference(subjects, completedSubjects);

    if (unfinishedSubjects.length === 0) {
      // finished this semester
    } else {
      // some subject in this semester is still not done
      // set level to this semester
      semesterWithUnfinishedSubjects =
        semesterWithUnfinishedSubjects !== 0 ? semesterWithUnfinishedSubjects : semesterIndex + 1;
      // if this semester is the first semester with unfinished subjects,
      if (semesterIndex === semesterWithUnfinishedSubjects - 1) {
        for await (const id of unfinishedSubjects) {
          const subject = await Subject.findById(id);
          const requirementSubjects = subject.prerequisites.subjects.map(member => member.toString());
          if (
            _.difference(requirementSubjects, completedSubjects).length === 0 &&
            subject.prerequisites.yearLevel <= Math.floor(semesterWithUnfinishedSubjects / 2) + 1
          ) {
            // if satisfied, push subject to array for subjects needed to be a regular student again
            subjectsForRegularStatus.push(subject);
          }
        }
      }
      // unfinishedSubjects array contains the id of the subject
      // for each subject inside...
      for await (const id of unfinishedSubjects) {
        const subject = await Subject.findById(id);
        // check if unfinished subject's requirements are satisfied
        const requirementSubjects = subject.prerequisites.subjects.map(member => member.toString());
        if (
          _.difference(requirementSubjects, completedSubjects).length === 0 &&
          subject.prerequisites.yearLevel <= Math.floor(semesterWithUnfinishedSubjects / 2) + 1
        ) {
          // if satisfied, push subject to array for subjects cleared to take
          subjectsToBeRecommended.push(subject);
        }
      }
    }
  }

  isRegular = semesterWithUnfinishedSubjects
    ? _.difference(
        curriculum.subjects[semesterWithUnfinishedSubjects - 1].map(subject => subject.toString()),
        subjectsForRegularStatus.map(subject => subject._id.toString()),
      ).length === 0
    : true;
  this.isRegular = isRegular;
  this.yearLevel = Math.floor(semesterWithUnfinishedSubjects / 2) + 1;
  this.save();
  if (this.isRegular) {
    return {
      subjects: subjectsForRegularStatus,
    };
  }
  return {
    subjects: subjectsToBeRecommended,
    regular: subjectsForRegularStatus,
  };
};

StudentSchema.pre(`remove`, async function deleteAssociatedFiles() {
  await File.findByIdAndRemove(this.attachment);
});

StudentSchema.plugin(uniqueValidator);

StudentSchema.set(`toJSON`, { virtuals: true });

module.exports = mongoose.model(`Student`, StudentSchema);
