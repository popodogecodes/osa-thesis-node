const mongoose = require(`mongoose`);

const titleCase = require(`../utils/firstLetterCapitalize`);

const incrementRevision = require(`./schemaMethods/incrementRevision`);

const GradeSchema = mongoose.Schema({
  prelim: {
    type: Number,
    min: 0,
    max: 100,
    default: 0,
  },

  midterm: {
    type: Number,
    min: 0,
    max: 100,
    default: 0,
  },

  preFinal: {
    type: Number,
    min: 0,
    max: 100,
    default: 0,
  },

  final: {
    type: Number,
    min: 0,
    max: 100,
    default: 0,
  },

  status: {
    type: String,
    default: `Pending`,
    trim: true,
    set: value => titleCase(value),
    enum: [`Pending`, `Completed`, `Incomplete`, `In-Progress`, `Dropped`, `Failed`],
  },

  class: { type: mongoose.Schema.Types.ObjectId, required: true },
  student: { type: mongoose.Schema.Types.ObjectId, required: true },
});

GradeSchema.virtual(`total`).get(function getTotalGrade() {
  return ((this.prelim * 2) / 10 + (this.midterm * 2) / 10 + (this.preFinal * 2) / 10 + (this.final * 4) / 10).toFixed(
    2,
  );
});

GradeSchema.pre(`save`, incrementRevision);

GradeSchema.set(`toJSON`, { virtuals: true });

module.exports = mongoose.model(`Grade`, GradeSchema);
