const mongoose = require(`mongoose`);

const incrementRevision = require(`./schemaMethods/incrementRevision`);

const CurriculumSchema = mongoose.Schema({
  year: {
    type: Number,
    required: true,
  },

  course: {
    type: String,
    required: true,
    uppercase: true,
    trim: true,
  },

  subjects: [[{ type: mongoose.Schema.Types.ObjectId }]],
});

CurriculumSchema.pre(`save`, incrementRevision);

CurriculumSchema.set(`toJSON`, { virtuals: true });

module.exports = mongoose.model(`Curriculum`, CurriculumSchema);
