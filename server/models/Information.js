const mongoose = require(`mongoose`);

const incrementRevision = require(`./schemaMethods/incrementRevision`);

const InformationSchema = mongoose.Schema({
  phoneNumber: {
    type: String,
    required: true,
  },

  website: {
    type: String,
    required: true,
  },

  address: {
    type: String,
    required: true,
  },
});

InformationSchema.pre(`save`, incrementRevision);

module.exports = mongoose.model(`Information`, InformationSchema);
