module.exports = async function generateDefaultPassword() {
  if (!this.password && this.isNew) {
    this.password = `this is a temporary password`;
  }
};
