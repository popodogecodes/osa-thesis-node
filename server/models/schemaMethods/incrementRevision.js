module.exports = async function incrementRevision() {
  if (!this.isNew) {
    this.increment();
  }
};
