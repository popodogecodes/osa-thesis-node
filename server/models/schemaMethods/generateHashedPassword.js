const bcrypt = require(`bcryptjs`);

module.exports = async function generateHashedPassword() {
  if (this.isModified(`password`)) {
    this.password = bcrypt.hashSync(this.password, 12);
  }
};
