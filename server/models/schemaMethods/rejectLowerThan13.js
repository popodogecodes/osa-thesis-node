const subtractMonthAndYearFromDate = require(`../../utils/subtractMonthAndYearFromDate`);

module.exports = function rejectLowerThan13(value) {
  const minimumAge = 13;
  const result = subtractMonthAndYearFromDate(new Date(), 0, minimumAge);

  return value.getTime() <= result.getTime();
};
