const mongoose = require(`mongoose`);
const uniqueValidator = require(`mongoose-unique-validator`);

const titleCase = require(`../utils/firstLetterCapitalize`);

const incrementRevision = require(`./schemaMethods/incrementRevision`);

const SubjectSchema = mongoose.Schema({
  name: {
    type: String,
    required: true,
    trim: true,
    set: value => titleCase(value),
  },

  classCode: {
    type: String,
    required: true,
    trim: true,
    uppercase: true,
  },

  isCountedGPA: {
    type: Boolean,
    required: true,
  },

  units: {
    lecture: {
      type: Number,
      required: true,
      min: 1,
    },
    laboratory: {
      type: Number,
      default: 0,
    },
  },

  prerequisites: {
    subjects: [{ type: mongoose.Schema.Types.ObjectId }],
    yearLevel: { type: Number, min: 1, default: 1 },
  },
});

SubjectSchema.plugin(uniqueValidator);

SubjectSchema.pre(`save`, incrementRevision);

SubjectSchema.set(`toJSON`, { virtuals: true });

module.exports = mongoose.model(`Subject`, SubjectSchema);
