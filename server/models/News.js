const mongoose = require(`mongoose`);

const File = require(`./Files`);

const incrementRevision = require(`./schemaMethods/incrementRevision`);

const NewsSchema = mongoose.Schema({
  title: {
    type: String,
    required: true,
  },

  createdAt: {
    type: Date,
    default: new Date(),
  },

  author: {
    type: mongoose.Schema.Types.ObjectId,
    required: true,
  },

  authorType: {
    type: String,
    required: true,
  },

  content: {
    type: String,
    required: true,
  },

  attachment: { type: mongoose.Schema.Types.ObjectId },

  isPrivate: {
    type: Boolean,
    default: true,
  },

  isStaffLevel: {
    type: Boolean,
    default: false,
  },

  isFacultyLevel: {
    type: Boolean,
    default: false,
  },

  forClass: [{ type: mongoose.Schema.Types.ObjectId }],

  forCourse: [{ type: String, trim: true }],
});

NewsSchema.pre(`save`, incrementRevision);

NewsSchema.pre(`remove`, async function deleteAssociatedFiles() {
  await File.findByIdAndRemove(this.attachment);
});

module.exports = mongoose.model(`News`, NewsSchema);
