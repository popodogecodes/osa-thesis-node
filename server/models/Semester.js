const mongoose = require(`mongoose`);

const incrementRevision = require(`./schemaMethods/incrementRevision`);

const SemesterSchema = mongoose.Schema({
  startDate: {
    type: Date,
    required: true,
  },

  endDate: {
    type: Date,
    required: true,
  },

  schoolYear: {
    type: Number,
    required: true,
    get: value => `${value} - ${value + 1}`,
  },

  instance: {
    type: Number,
    required: true,
    get: value => `Semester ${value}`,
  },

  classes: [{ type: mongoose.Schema.Types.ObjectId }],
});

SemesterSchema.pre(`save`, incrementRevision);

SemesterSchema.post(`save`, async () => {
  const Student = mongoose.model(`Student`);
  const students = await Student.find({ 'enrollmentStatus.isCurrentlyEnrolled': true });
  students.forEach(student => {
    student.enrollmentStatus.isCurrentlyEnrolled = false;
    student.enrollmentStatus.hasPaidPrelims = false;
    student.enrollmentStatus.hasPaidMidterms = false;
    student.enrollmentStatus.hasPaidFinals = false;
  });

  await Promise.all(students.map(student => student.save()));
});

SemesterSchema.set(`toJSON`, { virtuals: true });

module.exports = mongoose.model(`Semester`, SemesterSchema);
