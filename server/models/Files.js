const mongoose = require(`mongoose`);
const B2 = require(`backblaze-b2`);

const b2 = new B2({
  accountId: `4b7a2d824f3d`,
  applicationKey: `002a96e2471d3f91231cc2e55efea065feefb55934`,
});

const FilesSchema = mongoose.Schema({
  fileId: {
    type: String,
    required: true,
  },

  size: {
    type: Number,
    required: true,
  },

  fileName: {
    type: String,
    required: true,
  },
});

FilesSchema.pre(`remove`, async function deleteFilesFromBucket() {
  try {
    await b2.authorize();
    await b2
      .deleteFileVersion({
        fileId: this.fileId,
        fileName: this.fileName,
      })
      .then(res => res.data);
  } catch (err) {
    console.log(`something happened!: `, err);
  }
});

module.exports = mongoose.model(`File`, FilesSchema);
