const mongoose = require(`mongoose`);
const uniqueValidator = require(`mongoose-unique-validator`);
const validator = require(`mongoose-validator`);
const bcrypt = require(`bcryptjs`);

const titleCase = require(`../utils/firstLetterCapitalize`);
const CredentialsSchema = require(`./CredentialsSchema`);

const generateHashedPassword = require(`./schemaMethods/generateHashedPassword`);
const generateDefaultPassword = require(`./schemaMethods/generateDefaultPassword`);
const incrementRevision = require(`./schemaMethods/incrementRevision`);
const rejectLowerThan13 = require(`./schemaMethods/rejectLowerThan13`);

const TeacherSchema = mongoose.Schema({
  firstName: {
    type: String,
    required: [true, `First Name is required!`],
    trim: true,
    set: value => titleCase(value),
  },

  middleName: {
    type: String,
    required: [true, `Middle Name is required!`],
    trim: true,
    set: value => titleCase(value),
  },

  lastName: {
    type: String,
    required: [true, `Last Name is required!`],
    trim: true,
    set: value => titleCase(value),
  },

  birthday: {
    required: true,
    type: Date,
    validate: {
      validator: rejectLowerThan13,
      message: value => `${value} needs to be to have a computed age of 13 or bigger. `,
    },
  },

  gender: {
    required: true,
    type: String,
    enum: [`male`, `female`],
    lowercase: true,
    trim: true,
  },

  houseNumber: {
    required: true,
    type: String,
    trim: true,
  },

  street: {
    required: true,
    type: String,
    trim: true,
    set: value => titleCase(value),
  },

  barangay: {
    required: true,
    type: String,
    trim: true,
    set: value => titleCase(value),
  },

  city: {
    required: true,
    type: String,
    trim: true,
    set: value => titleCase(value),
  },

  province: {
    required: true,
    type: String,
    trim: true,
    set: value => titleCase(value),
  },

  civilStatus: {
    required: true,
    type: String,
    trim: true,
    enum: [`Single`, `Married`],
    set: value => titleCase(value),
  },

  classes: [{ type: mongoose.Schema.Types.ObjectId }],

  email: {
    type: String,
    required: [true, `email is required!`],
    unique: true,
    trim: true,
    lowercase: true,
    validate: validator({ validator: `isEmail`, message: `must provide a valid email address!` }),
  },

  password: {
    type: String,
  },

  credentials: [CredentialsSchema],

  department: {
    type: [String],
    trim: true,
  },

  isCurrentlyEmployed: {
    type: Boolean,
    default: true,
  },
});

TeacherSchema.pre(`save`, incrementRevision);
TeacherSchema.pre(`save`, generateDefaultPassword);
TeacherSchema.pre(`save`, generateHashedPassword);

TeacherSchema.virtual(`fullName`).get(function getFullName() {
  return `${this.lastName}, ${this.firstName} ${this.middleName}`;
});

TeacherSchema.virtual(`age`).get(function getAge() {
  const today = new Date();
  const birthDate = this.birthday;
  let age = today.getFullYear() - birthDate.getFullYear();
  const m = today.getMonth() - birthDate.getMonth();
  if (m < 0 || (m === 0 && today.getDate() < birthDate.getDate())) {
    age -= 1;
  }
  return age;
});

TeacherSchema.virtual(`address`).get(function getAddress() {
  return `${this.houseNumber} ${this.street} St., Brgy. ${this.barangay}, ${this.city}, ${this.province}`;
});

TeacherSchema.methods.validPassword = function comparePassword(password) {
  return bcrypt.compareSync(password, this.password);
};

TeacherSchema.plugin(uniqueValidator);

TeacherSchema.set(`toJSON`, { virtuals: true });

module.exports = mongoose.model(`Teacher`, TeacherSchema);
