const mongoose = require(`mongoose`);
const _ = require(`lodash`);

const incrementRevision = require(`./schemaMethods/incrementRevision`);

const SectionSchema = mongoose.Schema({
  number: {
    type: Number,
    default: 1,
  },

  semester: {
    type: mongoose.Schema.Types.ObjectId,
    required: true,
  },

  course: {
    type: String,
    uppercase: true,
    required: true,
    trim: true,
  },

  yearLevel: {
    type: Number,
    default: 1,
  },

  students: [{ type: mongoose.Schema.Types.ObjectId }],
});

SectionSchema.pre(`save`, incrementRevision);

SectionSchema.pre(`save`, async function assessEligibleStudentsIfNew() {
  if (this.isNew) {
    await this.assessEligibleStudents();
  }
});

SectionSchema.methods.assessEligibleStudents = async function assessEligibleStudents() {
  const Student = mongoose.model(`Student`);
  const Curriculum = mongoose.model(`Curriculum`);
  let curriculums = await Curriculum.find({ course: this.course });
  curriculums = curriculums.map(curriculum => curriculum._id);
  const students = await Student.find({ 'enrollmentStatus.isCurrentlyEnrolled': true })
    .where(`curriculum`)
    .in(curriculums);
  await Promise.all(students.map(student => student.assessment()));
};

SectionSchema.methods.findEligibleStudents = async function findEligibleStudents() {
  const Student = mongoose.model(`Student`);
  const Curriculum = mongoose.model(`Curriculum`);
  const students = await Student.find(
    {
      'enrollmentStatus.isCurrentlyEnrolled': true,
      yearLevel: this.yearLevel,
      isRegular: true,
    },
    `firstName middleName lastName`,
  ).populate({
    path: `curriculum`,
    model: Curriculum,
  });

  return students.filter(student => student.curriculum.course.toLowerCase() === this.course.toLowerCase());
};

SectionSchema.methods.addStudents = async function addStudents(...ids) {
  const Student = mongoose.model(`Student`);
  const studentIds = ids.map(id => mongoose.Types.ObjectId(id));
  const students = await Student.find()
    .where(`_id`)
    .in(studentIds);
  students.forEach(student => {
    student.isInSection = true;
  });
  await Promise.all(students.map(student => student.save()));
  this.students.push(studentIds);
  this.save();
};

SectionSchema.methods.removeStudents = async function removeStudents(...ids) {
  const Student = mongoose.model(`Student`);
  const studentIds = ids.map(id => mongoose.Types.ObjectId(id));
  const students = await Student.find()
    .where(`_id`)
    .in(studentIds);
  students.forEach(student => {
    student.isInSection = false;
  });
  await Promise.all(students.map(student => student.save()));
  const studentsInSection = this.students.map(student => student.toString());
  const studentsLeftInSection = _.difference(studentsInSection, ids);
  this.students = studentsLeftInSection.map(student => mongoose.Types.ObjectId(student));
  this.save();
};

module.exports = mongoose.model(`Section`, SectionSchema);
