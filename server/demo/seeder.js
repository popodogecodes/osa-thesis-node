const Student = require(`../models/Student`);
const Class = require(`../models/Class`);
const Teacher = require(`../models/Teacher`);
const Staff = require(`../models/Staff`);
const Semester = require(`../models/Semester`);
const Curriculum = require(`../models/Curriculum`);
const Grade = require(`../models/Grade`);
const Subject = require(`../models/Subject`);
const Info = require(`../models/Information`);
const News = require(`../models/News`);

const mongoose = require(`mongoose`);

mongoose.connect(`mongodb://localhost:27017/OSA`, {
  useCreateIndex: true,
  useNewUrlParser: true,
});

const curriculumIT = mongoose.Types.ObjectId();
const curriculumCS = mongoose.Types.ObjectId();
const student1Id = mongoose.Types.ObjectId();
const student2Id = mongoose.Types.ObjectId();
const student3Id = mongoose.Types.ObjectId();
const math101subject = mongoose.Types.ObjectId();
const econ101subject = mongoose.Types.ObjectId();
const computer101subject = mongoose.Types.ObjectId();
const math102subject = mongoose.Types.ObjectId();
const math201subject = mongoose.Types.ObjectId();
const ezsubject = mongoose.Types.ObjectId();
const math202subject = mongoose.Types.ObjectId();
const professor1Id = mongoose.Types.ObjectId();
const professor2Id = mongoose.Types.ObjectId();
const semester1Id = mongoose.Types.ObjectId();
const semester2Id = mongoose.Types.ObjectId();
const class1Id = mongoose.Types.ObjectId();
const class2Id = mongoose.Types.ObjectId();
const class3Id = mongoose.Types.ObjectId();
const class4Id = mongoose.Types.ObjectId();
const class5Id = mongoose.Types.ObjectId();
const grade1Id = mongoose.Types.ObjectId();
const grade2Id = mongoose.Types.ObjectId();
const grade3Id = mongoose.Types.ObjectId();
const grade4Id = mongoose.Types.ObjectId();
const grade5Id = mongoose.Types.ObjectId();
const grade6Id = mongoose.Types.ObjectId();
const grade7Id = mongoose.Types.ObjectId();
const grade8Id = mongoose.Types.ObjectId();
const grade9Id = mongoose.Types.ObjectId();
const grade10Id = mongoose.Types.ObjectId();

const seedInfo = async () => {
  await Info.create({
    phoneNumber: `(044) 486 4455`,
    website: `aclc.edu.ph`,
    address: `Maharlika Hi-Way, Sto. Niño, Gapan, Nueva Ecija 3105`,
  });
};

const seedNews = async () => {
  const staff = await Staff.findOne({});
  await News.create({
    title: `Public Lorem Ipsum`,
    content: `Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc omni virtuti vitium contrario nomine opponitur. Idne consensisse de Calatino plurimas gentis arbitramur, primarium populi fuisse, quod praestantissimus fuisset in conficiendis voluptatibus? Age nunc isti doceant, vel tu potius quis enim ista melius? Bonum incolumis acies: misera caecitas. Duo Reges: constructio interrete. Naturales divitias dixit parabiles esse, quod parvo esset natura contenta. Maximas vero virtutes iacere omnis necesse est voluptate dominante. Quid ad utilitatem tantae pecuniae? Non quaero, quid dicat, sed quid convenienter possit rationi et sententiae suae dicere. Num igitur utiliorem tibi hunc Triarium putas esse posse, quam si tua sint Puteolis granaria? 

Sit enim idem caecus, debilis. Cuius similitudine perspecta in formarum specie ac dignitate transitum est ad honestatem dictorum atque factorum. Aut unde est hoc contritum vetustate proverbium: quicum in tenebris? Certe non potest. Aliena dixit in physicis nec ea ipsa, quae tibi probarentur; Conferam tecum, quam cuique verso rem subicias;`,
    author: staff._id,
    authorType: `staff`,
    isPrivate: false,
  });

  await News.create({
    title: `Private Lorem Ipsum`,
    content: `Sit enim idem caecus, debilis. Cuius similitudine perspecta in formarum specie ac dignitate transitum est ad honestatem dictorum atque factorum. Aut unde est hoc contritum vetustate proverbium: quicum in tenebris? Certe non potest. Aliena dixit in physicis nec ea ipsa, quae tibi probarentur; Conferam tecum, quam cuique verso rem subicias; 

Dici enim nihil potest verius. Tum mihi Piso: Quid ergo? Ne amores quidem sanctos a sapiente alienos esse arbitrantur. In qua si nihil est praeter rationem, sit in una virtute finis bonorum; Ita fit beatae vitae domina fortuna, quam Epicurus ait exiguam intervenire sapienti. Progredientibus autem aetatibus sensim tardeve potius quasi nosmet ipsos cognoscimus. 
`,
    author: staff._id,
    authorType: `staff`,
    isPrivate: true,
  });
};

const makeStudents = async () => {
  await Student.create(
    {
      _id: student1Id,
      email: `teststudent1@test.com`,
      password: `student1`,
      firstName: `Ray`,
      middleName: `Is`,
      lastName: `Makinang`,
      birthday: new Date(1995, 2, 13),
      gender: `male`,
      houseNumber: `#111`,
      street: `Bago`,
      barangay: `Poblacion`,
      city: `Cabiao`,
      province: `Nueva Ecija`,
      civilStatus: `Single`,
      curriculum: curriculumIT,
      classes: [class1Id, class2Id, class4Id],
      credentials: { schoolName: `School is Cool`, yearGraduated: 2012, educationalLevel: `High School Graduate` },
    },
    {
      _id: student2Id,
      email: `teststudent2@test.com`,
      password: `student2`,
      firstName: `Annie`,
      middleName: `The`,
      lastName: `Bakal`,
      birthday: new Date(1996, 0, 28),
      gender: `Female`,
      houseNumber: `#155`,
      street: `Acacia`,
      barangay: `Maginaw`,
      city: `San Leonardo`,
      province: `Nueva Ecija`,
      civilStatus: `Single`,
      curriculum: curriculumIT,
      classes: [class1Id, class2Id, class4Id, class5Id],
      credentials: { schoolName: `School of Rock`, yearGraduated: 2012, educationalLevel: `High School Graduate` },
    },
    {
      _id: student3Id,
      email: `teststudent3@test.com`,
      password: `student3`,
      firstName: `Rocky`,
      middleName: `Bote`,
      lastName: `Dilat`,
      birthday: new Date(1994, 10, 2),
      gender: `male`,
      houseNumber: `#027`,
      street: `Happy`,
      barangay: `Ginebra`,
      city: `San Miguel`,
      province: `Bulacan`,
      civilStatus: `Single`,
      curriculum: curriculumCS,
      classes: [class1Id, class3Id, class4Id],
      credentials: { schoolName: `Matalinaw School`, yearGraduated: 2010, educationalLevel: `High School Graduate` },
    },
  );
};

const makeCurriculum = async () => {
  await Curriculum.create(
    {
      _id: curriculumIT,
      year: 2014,
      course: `BSIT`,
      subjects: [[math101subject, econ101subject], [math102subject], [math201subject], [math202subject, ezsubject]],
    },
    {
      _id: curriculumCS,
      year: 2014,
      course: `BSCS`,
      subjects: [[math101subject, computer101subject], [math102subject], [ezsubject]],
    },
  );
};

const makeSubjects = async () => {
  await Subject.create(
    {
      _id: math101subject,
      name: `College Algebra`,
      classCode: `GE105`,
      units: { lecture: 3, laboratory: 0 },
      isCountedGPA: true,
      prerequisites: {},
    },
    {
      _id: math102subject,
      name: `Intermediate Algebra`,
      classCode: `GE203`,
      units: { lecture: 3, laboratory: 0 },
      isCountedGPA: true,
      prerequisites: { subjects: [math101subject] },
    },
    {
      _id: econ101subject,
      name: `College Economics`,
      classCode: `GE107`,
      units: { lecture: 3, laboratory: 0 },
      isCountedGPA: true,
      prerequisites: {},
    },
    {
      _id: computer101subject,
      name: `Computer Basics on Business Applications`,
      classCode: `GE102`,
      units: { lecture: 3, laboratory: 0 },
      isCountedGPA: true,
      prerequisites: {},
    },
    {
      _id: math201subject,
      name: `Differential Calculus`,
      classCode: `GE201`,
      units: { lecture: 5, laboratory: 0 },
      isCountedGPA: true,
      prerequisites: { subjects: [math102subject], yearLevel: 2 },
    },
    {
      _id: math202subject,
      name: `Integral Calculus`,
      classCode: `GE211`,
      units: { lecture: 5, laboratory: 0 },
      isCountedGPA: true,
      prerequisites: { subjects: [math201subject], yearLevel: 2 },
    },
    {
      _id: ezsubject,
      name: `EZ Subject`,
      classCode: `GE200`,
      units: { lecture: 3, laboratory: 0 },
      isCountedGPA: true,
      prerequisites: { subjects: [], yearLevel: 2 },
    },
  );
};

const makeProfessor = async () => {
  await Teacher.create(
    {
      _id: professor1Id,
      email: `testteacher1@test.com`,
      password: `teacher1`,
      firstName: `Brainy`,
      middleName: `Bautista`,
      lastName: `Bote`,
      birthday: new Date(1982, 2, 3),
      gender: `male`,
      houseNumber: `#100`,
      street: `Rizal`,
      barangay: `Poblacion Central`,
      city: `Penaranda`,
      province: `Nueva Ecija`,
      civilStatus: `Married`,
      classes: [class1Id, class2Id, class4Id, class5Id],
      credentials: { schoolName: `Hard Skool`, yearGraduated: 2000, educationalLevel: `BS Math Graduate` },
      department: [`IT`],
    },
    {
      _id: professor2Id,
      email: `testteacher2@test.com`,
      password: `teacher2`,
      firstName: `Shades`,
      middleName: `Gomez`,
      lastName: `Diaz`,
      birthday: new Date(1980, 0, 2),
      gender: `male`,
      houseNumber: `#172`,
      street: `Mabini`,
      barangay: `Maginhawa`,
      city: `Jaen`,
      province: `Nueva Ecija`,
      civilStatus: `Single`,
      classes: [class3Id],
      credentials: { schoolName: `Cool Boys School`, yearGraduated: 1998, educationalLevel: `BS IT Graduate` },
      department: [`IT`],
    },
  );
};

const makeSemester = async () => {
  await Semester.create(
    {
      _id: semester1Id,
      startDate: new Date(2015, 6, 1),
      endDate: new Date(2015, 9, 28),
      schoolYear: 2015,
      instance: 1,
      classes: [],
    },
    {
      _id: semester2Id,
      startDate: new Date(2015, 10, 15),
      endDate: new Date(2016, 3, 20),
      schoolYear: 2015,
      instance: 2,
      classes: [],
    },
  );
};

const makeClasses = async () => {
  await Class.create({
    _id: class1Id,
    professor: professor1Id,
    subject: math101subject,
    schedule: {
      lecture: [
        { time: `15-16:30`, room: `rm302`, day: `Tuesday` },
        { time: `11-12:30`, room: `rm302`, day: `Thursday` },
      ],
    },
    attendees: [
      { student: student1Id, grade: grade1Id },
      { student: student2Id, grade: grade2Id },
      { student: student3Id, grade: grade3Id },
    ],
    semester: semester1Id,
  });
  await Class.create({
    _id: class2Id,
    professor: professor1Id,
    subject: econ101subject,
    schedule: { lecture: [{ time: `9-12`, room: `rm201`, day: `Monday` }] },
    attendees: [{ student: student1Id, grade: grade4Id }, { student: student2Id, grade: grade5Id }],
    semester: semester1Id,
  });
  await Class.create({
    _id: class3Id,
    professor: professor2Id,
    subject: computer101subject,
    schedule: { lecture: [{ time: `13-16`, room: `rm102`, day: `Friday` }] },
    attendees: [{ student: student3Id, grade: grade6Id }],
    semester: semester1Id,
  });
  await Class.create({
    _id: class4Id,
    professor: professor1Id,
    subject: math102subject,
    schedule: {
      lecture: [
        { time: `13-14:30`, room: `rm101`, day: `Monday` },
        { time: `13-14:30`, room: `rm101`, day: `Thursday` },
      ],
    },
    attendees: [
      { student: student1Id, grade: grade7Id },
      { student: student2Id, grade: grade8Id },
      { student: student3Id, grade: grade9Id },
    ],
    semester: semester2Id,
  });
  await Class.create({
    _id: class5Id,
    professor: professor1Id,
    subject: econ101subject,
    schedule: { lecture: [{ time: `15-16:30`, room: `rm302`, day: `Wednesday` }] },
    attendees: [{ student: student2Id, grade: grade10Id }],
    semester: semester2Id,
  });
};

const makeGrades = async () => {
  await Grade.create(
    {
      _id: grade1Id,
      prelim: Math.floor(Math.random() * (100 - 50 + 1) + 50),
      midterm: Math.floor(Math.random() * (100 - 50 + 1) + 50),
      preFinal: Math.floor(Math.random() * (100 - 50 + 1) + 50),
      final: Math.floor(Math.random() * (100 - 50 + 1) + 50),
      student: student1Id,
      class: class1Id,
      status: `Completed`,
    },
    {
      _id: grade2Id,
      prelim: Math.floor(Math.random() * (100 - 50 + 1) + 50),
      midterm: Math.floor(Math.random() * (100 - 50 + 1) + 50),
      preFinal: Math.floor(Math.random() * (100 - 50 + 1) + 50),
      final: Math.floor(Math.random() * (100 - 50 + 1) + 50),
      student: student2Id,
      class: class1Id,
      status: `Completed`,
    },
    {
      _id: grade3Id,
      prelim: Math.floor(Math.random() * (100 - 50 + 1) + 50),
      midterm: Math.floor(Math.random() * (100 - 50 + 1) + 50),
      preFinal: Math.floor(Math.random() * (100 - 50 + 1) + 50),
      final: Math.floor(Math.random() * (100 - 50 + 1) + 50),
      student: student3Id,
      class: class1Id,
      status: `Completed`,
    },
    {
      _id: grade4Id,
      prelim: Math.floor(Math.random() * (100 - 50 + 1) + 50),
      midterm: Math.floor(Math.random() * (100 - 50 + 1) + 50),
      preFinal: Math.floor(Math.random() * (100 - 50 + 1) + 50),
      final: Math.floor(Math.random() * (100 - 50 + 1) + 50),
      student: student1Id,
      class: class2Id,
      status: `Completed`,
    },
    {
      _id: grade5Id,
      prelim: Math.floor(Math.random() * (60 - 10 + 1) + 10),
      midterm: Math.floor(Math.random() * (60 - 10 + 1) + 10),
      preFinal: Math.floor(Math.random() * (60 - 10 + 1) + 10),
      final: Math.floor(Math.random() * (60 - 10 + 1) + 10),
      student: student2Id,
      class: class2Id,
      status: `Failed`,
    },
    {
      _id: grade6Id,
      prelim: Math.floor(Math.random() * (100 - 50 + 1) + 50),
      midterm: Math.floor(Math.random() * (100 - 50 + 1) + 50),
      preFinal: Math.floor(Math.random() * (100 - 50 + 1) + 50),
      final: Math.floor(Math.random() * (100 - 50 + 1) + 50),
      student: student3Id,
      class: class3Id,
      status: `Completed`,
    },
    {
      _id: grade7Id,
      prelim: Math.floor(Math.random() * (100 - 50 + 1) + 50),
      midterm: Math.floor(Math.random() * (100 - 50 + 1) + 50),
      preFinal: Math.floor(Math.random() * (100 - 50 + 1) + 50),
      final: Math.floor(Math.random() * (100 - 50 + 1) + 50),
      student: student1Id,
      class: class4Id,
      status: `Completed`,
    },
    {
      _id: grade8Id,
      prelim: Math.floor(Math.random() * (100 - 50 + 1) + 50),
      midterm: Math.floor(Math.random() * (100 - 50 + 1) + 50),
      preFinal: Math.floor(Math.random() * (100 - 50 + 1) + 50),
      final: Math.floor(Math.random() * (100 - 50 + 1) + 50),
      student: student2Id,
      class: class4Id,
      status: `Completed`,
    },
    {
      _id: grade9Id,
      prelim: Math.floor(Math.random() * (100 - 50 + 1) + 50),
      midterm: Math.floor(Math.random() * (100 - 50 + 1) + 50),
      preFinal: Math.floor(Math.random() * (100 - 50 + 1) + 50),
      final: Math.floor(Math.random() * (100 - 50 + 1) + 50),
      student: student3Id,
      class: class4Id,
      status: `Completed`,
    },
    {
      _id: grade10Id,
      prelim: Math.floor(Math.random() * (100 - 50 + 1) + 50),
      midterm: Math.floor(Math.random() * (100 - 50 + 1) + 50),
      preFinal: Math.floor(Math.random() * (100 - 50 + 1) + 50),
      final: Math.floor(Math.random() * (100 - 50 + 1) + 50),
      student: student2Id,
      class: class5Id,
      status: `Failed`,
    },
  );
};

const makeStaff = async () => {
  await Staff.create(
    {
      email: `testregistrar@test.com`,
      password: `registrar`,
      firstName: `Betty`,
      middleName: `Pajarillaga`,
      lastName: `Suarez`,
      birthday: new Date(1986, 5, 20),
      gender: `female`,
      houseNumber: `#22`,
      street: `Lipa`,
      barangay: `Bubok`,
      city: `Cabanatuan`,
      province: `Nueva Ecija`,
      civilStatus: `Married`,
      credentials: { schoolName: `Tuff Skool`, yearGraduated: 2006, educationalLevel: `BS Accountancy Graduate` },
      role: `Registrar`,
    },
    {
      email: `testhead@test.com`,
      password: `head`,
      firstName: `Gibo`,
      middleName: `Perez`,
      lastName: `Santiago`,
      birthday: new Date(1976, 1, 25),
      gender: `female`,
      houseNumber: `#5`,
      street: `Narra`,
      barangay: `Verde`,
      city: `San Jose`,
      province: `Nueva Ecija`,
      civilStatus: `Married`,
      credentials: {
        schoolName: `Prestigious Skool`,
        yearGraduated: 1996,
        educationalLevel: `BS Business Administration Graduate`,
      },
      role: `Program Head`,
    },
    {
      email: `testcashier@test.com`,
      password: `cashier`,
      firstName: `Pinky`,
      middleName: `Santos`,
      lastName: `Pajimna`,
      birthday: new Date(1989, 3, 3),
      gender: `female`,
      houseNumber: `#12`,
      street: `Boondok`,
      barangay: `Pantal`,
      city: `Cabanatuan`,
      province: `Nueva Ecija`,
      civilStatus: `Single`,
      credentials: { schoolName: `Gud Skool`, yearGraduated: 2009, educationalLevel: `BS Accountancy Graduate` },
      role: `Cashier`,
    },
    {
      email: `testdirector@test.com`,
      password: `director`,
      firstName: `Janice`,
      middleName: `Rivera`,
      lastName: `Bulacan`,
      birthday: new Date(1981, 9, 6),
      gender: `female`,
      houseNumber: `#108`,
      street: `Luwas`,
      barangay: `Tabing Ilog`,
      city: `Palayan`,
      province: `Nueva Ecija`,
      civilStatus: `Married`,
      credentials: [
        { schoolName: `Gud Skool`, yearGraduated: 2001, educationalLevel: `BS Computer Science Graduate` },
        { schoolName: `Very Gud Skool`, yearGraduated: 2004, educationalLevel: `Master of Computer Science Graduate` },
      ],
      role: `School Director`,
    },
  );
};

makeSemester().then(() =>
  Promise.all([
    seedInfo(),
    makeStudents(),
    makeCurriculum(),
    makeGrades(),
    makeClasses(),
    makeStaff(),
    makeSubjects(),
    makeProfessor(),
  ])
    .then(() => seedNews())
    .then(() => console.log(`demo database stored.`)),
);
