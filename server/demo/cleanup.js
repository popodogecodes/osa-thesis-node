// const Student = require(`../models/Student`);
// const Class = require(`../models/Class`);
// const Teacher = require(`../models/Teacher`);
// const Staff = require(`../models/Staff`);
// const Semester = require(`../models/Semester`);
// const Curriculum = require(`../models/Curriculum`);
// const Grade = require(`../models/Grade`);
// const Subject = require(`../models/Subject`);

const mongoose = require(`mongoose`);

mongoose.connect(`mongodb://localhost:27017/OSA`, {
  useCreateIndex: true,
  useNewUrlParser: true,
});

// const {
//   classes,
//   curriculums,
//   grades,
//   semesters,
//   staffs,
//   students,
//   subjects,
//   teachers,
// } = mongoose.connection.collections;

Promise.all([
  mongoose.connection.dropCollection(`classes`),
  mongoose.connection.dropCollection(`curriculums`),
  mongoose.connection.dropCollection(`grades`),
  mongoose.connection.dropCollection(`semesters`),
  mongoose.connection.dropCollection(`staffs`),
  mongoose.connection.dropCollection(`students`),
  mongoose.connection.dropCollection(`subjects`),
  mongoose.connection.dropCollection(`teachers`),
]).then(() => mongoose.connection.dropDatabase().then(() => console.log(`database cleanup done!`)));

// mongoose.connection.dropCollection(`classes`).then(() => {
//   mongoose.connection.deleteModel(`Class`);
//   console.log(`class cleanup done!`);
// });

// Promise.all([
//   classes.drop(),
//   curriculums.drop(),
//   grades.drop(),
//   semesters.drop(),
//   staffs.drop(),
//   students.drop(),
//   subjects.drop(),
//   teachers.drop(),
// ]).then(() => mongoose.connection.dropDatabase().then(() => console.log(`Database cleanup done...`)));

// Promise.all([
//   Class.find({}).deleteMany(),
//   Student.find({}).deleteMany(),
//   Teacher.find({}).deleteMany(),
//   Staff.find({}).deleteMany(),
//   Semester.find({}).deleteMany(),
//   Curriculum.find({}).deleteMany(),
//   Grade.find({}).deleteMany(),
//   Subject.find({}).deleteMany(),
// ]).then(() => {
//   console.log(`Database cleanup done...`);
// });
