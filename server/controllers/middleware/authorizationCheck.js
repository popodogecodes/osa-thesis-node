module.exports = (req, res, next) => {
  if (req.userGroup === req.session.group) {
    next();
  } else {
    req.flash(`error`, `You're not authorized to access this resource!`);
    req.flash(`statusCode`, 403);
    res.redirect(`/${req.userGroup}/login`);
  }
};
