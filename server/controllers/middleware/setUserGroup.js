module.exports = group => (req, res, next) => {
  req.userGroup = group;
  next();
};
