module.exports = (req, res, next) => {
  if (req.isAuthenticated()) {
    next();
  } else {
    req.flash(`error`, `You're not logged in!`);
    req.flash(`statusCode`, 401);
    res.redirect(`/${req.userGroup}/login`);
  }
};
