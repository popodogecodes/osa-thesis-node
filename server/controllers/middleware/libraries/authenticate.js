const passport = require(`passport`);

module.exports = (req, res, next) => {
  passport.authenticate(`${req.userGroup}`, (err, user, info) => {
    if (info) {
      return res.status(401).send();
    }
    if (err) {
      console.log(`err occurred`);
      next(err);
    } else if (!user) {
      req.flash(`statusCode`, 401);
      return res.status(401).redirect(`/${req.userGroup}/login`);
    }
    return req.login(user, error => {
      if (err) {
        return next(error);
      }
      return res.json(user.toObject());
    });
  })(req, res, next);
};
