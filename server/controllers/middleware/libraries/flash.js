module.exports = (req, res, next) => {
  req.flash = (key, value) => {
    if (value) {
      if (!req.session.flash) {
        req.session.flash = {};
      }

      req.session.flash[key] = value;
      return req.session.flash[key];
    }
    return res.locals.flash[key];
  };

  res.locals.flash = req.session.flash || {};
  delete req.session.flash;

  return next();
};
