module.exports = (req, res, next) => {
  const requesterId = req.user.id.toString();
  res.locals.queryOptions = {
    student: `firstName lastName middleName`,
    teacher: `firstName lastName middleName`,
    staff: `firstName lastName middleName role`,
  };

  if (req.userGroup === `student` && requesterId === req.params.id) {
    res.locals.queryOptions.student = `-password`;
  } else if (req.userGroup === `faculty` && requesterId === req.params.id) {
    res.locals.queryOptions.teacher = `-password`;
  } else if (req.userGroup === `staff`) {
    if (requesterId === req.params.id) {
      res.locals.queryOptions.staff = `-password`;
    }
    if (req.user.role === `Registrar`) {
      res.locals.queryOptions.student = `-password`;
      if (requesterId === req.params.id) {
        res.locals.queryOptions.staff = `-password`;
      }
    } else if (req.user.role === `Program Head`) {
      res.locals.queryOptions.teacher = `-password`;
      if (requesterId === req.params.id) {
        res.locals.queryOptions.staff = `-password`;
      }
    } else if (req.user.role === `School Director`) {
      res.locals.queryOptions.staff = `-password`;
    } else if (req.user.role === `Cashier`) {
      res.locals.queryOptions.student = `firstName lastName middleName enrollmentStatus`;
    }
  }
  next();
};
