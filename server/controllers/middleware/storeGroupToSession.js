module.exports = (req, res, next) => {
  req.session.group = req.userGroup;
  next();
};
