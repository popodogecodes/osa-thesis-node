module.exports = (req, res) => {
  if (req.flash(`error`)) {
    return res.status(req.flash(`statusCode`)).send(`You got an error! ${req.flash(`error`)}`);
  }
  return res.send(`You got the ${req.userGroup}'s login page!`);
};
