const schedule = require(`schedulejs`);
const later = require(`later`);
const laterParser = later.parse.text;

schedule.date.localTime();

module.exports = (req, res) => {
  const payload = req.body;

  const tasks = [];
  const resources = [];
  let timeOpen = `on `;

  payload.schedule.forEach((duration, index) => {
    duration.days.forEach((day, innerIndex) => {
      if (innerIndex === duration.days.length - 1) {
        timeOpen += `${day} `;
      } else {
        timeOpen += `${day}, `;
      }
    });
    timeOpen += `after ${duration.period.after} before ${duration.period.before} `;
    if (index < payload.schedule.length - 1) {
      timeOpen += `also on `;
    }
  });

  timeOpen += `before 9 day of June in 2019`;

  payload.tasks.forEach(member => {
    for (let division = 1; division <= member.meetings; division += 1) {
      const task = {};
      task.id = `${member.subject}|Meeting ${division}`;
      task.duration = member.duration / member.meetings;
      task.minSchedule = task.duration;
      task.resources = member.requirements;
      task.priority = member.priority + 90 - 10 * (division - 1);
      if (division > 1) {
        task.dependsOn = [`${member.subject}|Meeting ${division - 1}`];
      }
      tasks.push(task);
    }
  });

  payload.resources.forEach(member => {
    const resource = {};
    resource.id = member.id;
    let availability = `on `;
    member.available.forEach((duration, index) => {
      duration.days.forEach((day, innerIndex) => {
        if (innerIndex === duration.days.length - 1) {
          availability += `${day} `;
        } else {
          availability += `${day}, `;
        }
      });
      availability += `after ${duration.period.after} before ${duration.period.before} `;
      if (index < member.available.length - 1) {
        availability += `also on `;
      }
    });
    resource.available = laterParser(availability);
    resources.push(resource);
  });

  const data = schedule.create(tasks, resources, laterParser(timeOpen), new Date(`June 2, 2019`));

  res.json(data);
};
