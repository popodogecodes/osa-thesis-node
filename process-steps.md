## 1. GENERAL PROCESSES

#### 1.1 User Authentication:

1. Hit Homepage
1. Select Which Group
1. Authenticate
1. Get sent to private page /w sessions

### 1.2 User CRUD

#### 1.2.1 User Registration (Create)

1. Hit Homepage
1. Login as Program Head (Faculty Registration), Registrar (Student Registration), and School Director (Staff Registration)
1. Head to creation page
1. Create user (DB I/O)

#### 1.2.2 User Lookup (Read)

1. Hit Homepage
1. Login as Program Head (Faculty Registration), Registrar (Student Registration), and School Director (Staff Registration)
1. Head to Lookup page
1. Search a user / users (DB I/O)

#### 1.2.3 User Editing (Update)

1. Hit Homepage
1. Login as Program Head (Faculty Registration), Registrar (Student Registration), and School Director (Staff Registration)
1. Head to Lookup page
1. Search a user / users (DB I/O)
1. Press a button in a user's profile to edit (DB I/O)

#### OR

1. Hit Homepage
1. Do User Authentication
1. Select Profile page (DB I/O)
1. Press a button inside profile to edit (DB I/O)

#### 1.2.4 User Editing (Delete)

1. Hit Homepage
1. Login as Program Head (Faculty Registration), Registrar (Student Registration), and School Director (Staff Registration)
1. Head to Lookup page
1. Search a user / users (DB I/O)
1. Press a button in a user's profile to delete (DB I/O)

### 1.3 Subjects CRUD

#### 1.3.1 Creating Subjects (Create)

1. Hit Homepage
1. Login as Program Head
1. Head to Subjects page (DB I/O)
1. Head to create a Subject page
1. Create subject (DB I/O)

#### 1.3.2 Looking up Subjects (Read)

1. Hit Homepage
1. Login as Program Head
1. Head to Subjects page (DB I/O)
1. Head to search a Subject page
1. Search subject (DB I/O)

#### 1.3.3 Update Subjects (Update)

1. Hit Homepage
1. Login as Program Head
1. Head to Subjects page (DB I/O)
1. Head to search a Subject page
1. Search subject (DB I/O)
1. Display result subject
1. Press update button in subject

#### 1.3.4 Delete Subjects (Delete)

1. Hit Homepage
1. Login as Program Head
1. Head to Subjects page (DB I/O)
1. Head to search a Subject page
1. Search subject (DB I/O)
1. Display result subject
1. Press delete button in subject

### 1.4 Semesters CRUD

#### 1.4.1 Creating an instance of a semester (Create)

1. Hit Homepage
1. Login as the School Director
1. Press the Semesters Button

#### 1.4.2 Searching / Reading an instance of a semester (Read)

#### 1.4.3 Updating an instance of a semester (Update)

#### 1.4.4 Deleting an instance of a semester (Delete)

### 1.5 Schedules

### 1.5.1 Creating a Schedule

1. Hit Homepage

### Grades CRUD

#### View Grades as a Student (Read)

1. Hit Homepage
1. Login as a Student
1. Head to profile (DB I/O)
1. Click button to show ALL grades (DB I/O)
1. Provide Search Parameters (by Subject or by Semester)
1. Show Specific Grade that satisfies arguments

#### View Grades of a Student as a Faculty (Read)

1. Hit Homepage
1. Login as a Faculty Member
1. Head to profile (DB I/O)
1. Click button to show ALL Classes (Display Semester, Then Class)
1. Click button to display contents of a particular class (Collection of students, with their grades)
1. Click student to view page with the student and their grade

####

## 2. Pre - Enrollment Processes

#### 2.1 Getting an old student's assessment

1. Hit Homepage
1. Login as Student / Registrar
1. Head to student's profile page
1. Click Get Assessment (DB I/O)

### Semester instances

#### 2.2.1 Create a Semester Instance (Create)

1. Hit Homepage
1. Login as School Director
1. Click on semesters page
1. Display all semesters (DB I/O)
1. Click create semester (DB I/O)

#### 2.2.2 Read / Search a Semester Instance's Details (Read)

1. Hit Homepage
1. Login as School Director
1. Click on semesters page
1. Display all semesters (DB I/O)
1. Search a semester using parameters
1. Click on result to view in detail

#### 2.2.3 Update a Semester Instance's Details (Update)

1. Hit Homepage
1. Login as School Director
1. Click on semesters page
1. Display all semesters (DB I/O)
1. Search a semester using parameters
1. Click on result to view in detail
1. Click the update semester button to edit

#### 2.2.4 Delete a Semester Instance's Details (Delete)

1. Hit Homepage
1. Login as School Director
1. Click on semesters page
1. Display all semesters (DB I/O)
1. Search a semester using parameters
1. Click on result to view in detail
1. Click the delete semester button to delete (with confirmation)

### 2.3 Schedules

#### 2.3.1 Creating a Schedule

1. Hit Homepage
1. Login as Program Head
1. Click on Create a Schedule
1. Provide Stuff

### 2.4 Class Instances

##### 2.4.1 Creating Classes (Create)

1. Hit Homepage
1. Login as Program Head
1. Click on Semesters page
1. Display all semesters
1. Search a semester using parameters
1. Click on result to view a single semester
1. Click on classes button for this semester
1. View all classes present for this semester
1. Click add classes

##### 2.4.1 Creating Classes ()

1. Hit Homepage
1. Login as Program Head
1. Click on Semesters page
1. Display all semesters
1. Search a semester using parameters
1. Click on result to view a single semester
1. Click add classes
