#### Users DB

    consists of several collections, with varying access levels: Teachers, Students, Admin

- Teachers

  - Unique ID
  - Basic Profile Bullshit
  - Current Classes (?)

- Students

  - Unique ID
  - Basic Profile Bullshit
  - Standing Regular / Irregular
  - Enrolled (Boolean)
  - Curriculum Year (Reference to Curriculum DB)
  - Current Classes ()
  - Grades (From previous classes)
    - Unique Subject ID reference

- Admin
  - Unique ID
  - Basic Profile Bullshit

#### Subjects DB

    consists of 2 Collections: Subjects, Curriculum (both new and old)

- Subjects

  - GPA Counted
    - Unique ID
    - Name
    - Class Code
    - Lecture Units
    - Laboratory Units
    - Prerequisites
  - Not Counted
    - Unique ID
    - Name
    - Class Code
    - Units
    - Prerequisites

- Curriculum
  - Unique ID
  - (Group by Year Standing, Semester)

#### Classes DB

- Year
- Semester
- Teacher (Referred by Teacher ID)
- Students (Array)
  - Reference Student ID
  - Grade (Object)
    - Prelim
    - Midterm
    - Pre - final
    - Final Grade
