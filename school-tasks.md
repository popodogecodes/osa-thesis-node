## A timeline for processes and tasks that happens in a school period

### General Processes

1. _Creating Subjects_
1. _Creating Users_
1.

### Pre - Enrollment Period

1. ~~Assessment for Old Students~~
1. _Defining the new Semester_
1. _Creating a schedule_
1. _Creating classes for the semester_

### Enrollment Period

Enrolling

- New Student
- Transfer Student
- Old Student
